# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<details>
<summary> How to write the changelog </summary>

### Guiding Principles

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

### Types of changes

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

</details>

## [0.4.3] – 2025-01-09

### Added

- Support for Python 3.12 and 3.13

### Removed

- Support for Python below 3.10

## [0.4.2] – 2023-10-18

### Added

- Support for Python 3.11

### Changed

- `pandas.read_csv` does not support `prefix` anymore. As a consequence the importers don't include the filenames in unnamed columns anymore.

## [0.4.1] – 2023-03-20

### Fixed

- Made the `analysis.cavity.finesse` script more robust.
- `openqlab.io.read` now allows a broader range of input objects such as `tarfile.ExFileObject`

### Added

- CONTRIBUTING.md
- R&S oscilloscope Visa importer

### Changed

- README.md
- LICENSE

## [0.4.0] – 2022-01-20

### Changed

- make `tables` an optional dependency that can be installed with `pip install openqlab[hdf]` for HDF support.
- make `DataContainer` use the official inheritance mechanism of `pandas >=v1.1`
- minimum requeired `pandas` version is now `1.3`
- make `visa` an optional dependency that can be installed with `pip install openqlab[visa]` for VISA support.
- use `OrderedDict` in the header of `DataContainer` to obtain the comment order of data files.

### Fixed

- `conversion.db.subtract` now works with `DataContainer`.
- `analysis.gaussian_beam.fit_beam_data` now works with `pandas.DataFrame`.
- `plots.frequency_domain.power_spectrum`: fixed inconsistent grid lines.
- Type fixes in `analysis.ServoDesign`.

### Added

- PID parameter calculator: `control.ziegler_nichols_method`.
- Allow arrays in `ElectromagneticWave`.
- `openqlab.io.data_container.DataContainerSeries` to fit the official inheritance mechanism of `pandas >=v1.1`
- `openqlab.spacing` module with functions from numpy to create regular spaced arrays.
  - `stepspac` function that accepts a `step` and `num` argument.
- Let `analysis.squeezing.max` calculate squeezing and antisqueezing values.

## [0.3.6] – 2021-04-16

### Added

- `plots.zero_span` can now output a time trace without decibel values.

### Fixed

- Removed unnecessary typechecking warnings.

## [0.3.5] – 2021-03-25

### Added

- `analysis.cavity` scripts now support an `Axes` as parameter

### Fixed

- `DataContainer.to_csv(header=False)` bug fixed, where the columns names were still saved to csv.

## [0.3.4] – 2021-03-12

### Added

- In `plots.zero_span` the number of digits for the calculated dB values can now be changed with a parameter.

### Fixed

- `DataContainer.to_csv` bug with kwargs

## [0.3.3] – 2021-02-03

### Added

- `KeysightVisa`: `io.read(..., stop_scope=True, number_of_points=1000)`
  - Parameter `stop_scope` to skip stopping the scope
  - Parameter `number_of_points` to change the number of datapoints

### Fixed

- Removed unnecessary `print` from `db.from_lin`.

### Fixed

- `plots.time_domain.zero_span`: avoid clipping of labels.

## [0.3.2] – 2020-11-25

### Added

- Python 3.9 compatibility
- If the package `uncertainties` is installed, `analysis.squeezing` now accepts values with uncertainty.

### Removed

- Unused dependencies

## [0.3.1] – 2020-10-28

### Fixed

- `nqlab` version update did not sort.

## [0.3] – 2020-10-20

### Added

- Run-time type checks for the end user to get better exceptions if the input data has wrong types.
- Visa importer for Rohde&Schwarz spectrometers

### Fixed

- `conf.py` for `sphinx` gets version from pyproject.toml and shows correct version in produced docs.

## [0.2.2] – 2020-08-21

### Fixed

- version of nqlab was not updated

## [0.2.1] – 2020-08-21

### Added

- Squeezing calculations:
  - losses from squeezing, anti-squeezing
  - initial squeezing
  - maximum possible squeezing from losses

## [0.2.0] – 2020-08-18

### Added

- Allow commenting out data lines in ASCII files with header.

### Changed

- Update to Pandas 1.1

## [0.1.18] – 2020-08-14

### Fixed

- Pinned Pandas to `< 1.1` to avoid some update problems.

## [0.1.17] – 2020-08-12

### Changed

- `fit_beam_data` now requires to specify the wavelength and does not use a default value of 1064 nm anymore.
- `fit_beam_data` returns a `pandas.DataFrame` with the fit parameters AND the errors instead of a `GaussianBeam`
- `fit_beam_data` can now be prevented from printing the results with the flag `print_results=False`. Default is `print_results=True`. The print format also changed for readablity.

## [0.1.16] – 2020-05-15

### Added

- Automatic tag generation process.

## [0.1.15] – 2020-05-15

### Added

- Introduced a changelog

### Fixed

- `fit_beam_data` now accepts columns with `NaN` values.
- `ascii_header` importer can now import plain ascii files with and without column names.
  Previously, the first data line was taken as column names.

## [0.1.14] – 2020-04-17

### Fixed

- Documentation compilation
