FROM python:3.8
ENV PYTHONUNBUFFERED 1
ENV BUILD_PACKAGES pandoc rsync openssh-client build-essential

RUN mkdir /code
WORKDIR /code

RUN apt-get update -y && apt-get install -y $BUILD_PACKAGES

RUN pip install pipenv
ADD Pipfile /code/
RUN pipenv install --dev && pipenv --rm && pipenv install --dev --system --deploy && rm -r /root/.cache/pip*
