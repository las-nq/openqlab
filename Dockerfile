ARG PYTHON_VERSION=3.11

FROM python:$PYTHON_VERSION

ENV PYTHONUNBUFFERED 1
ENV REBUILD 1

RUN mkdir /code
WORKDIR /code

ENV BUILD_PACKAGES pandoc rsync openssh-client build-essential hdf5-tools

RUN apt-get update -y
RUN apt-get install -y $BUILD_PACKAGES


RUN pip install --upgrade --no-cache poetry pip

RUN poetry config virtualenvs.create false

COPY pyproject.toml poetry.lock /code/
RUN poetry install -vvv --no-interaction --no-root --extras "hdf visa"
