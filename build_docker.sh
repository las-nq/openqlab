#!/usr/bin/env bash
set -e

project=openqlab

docker build -t $project .
docker tag $project lasnq/$project
docker push lasnq/$project
