all: doc bdist
doc:
	@cd doc; poetry run make html

bdist:
	poetry build

test:
	poetry run py.test -x

mypy:
	poetry run mypy src

pylint:
	poetry run pylint src/openqlab

black:
	poetry run black src

pydocstyle:
	poetry run pydocstyle --convention=numpy src/openqlab

flake8:
	poetry run flake8 src/openqlab

all-tests: mypy test pylint

container:
	bin/build_container.sh

clean:
	@rm -rf dist/
	cd doc; make clean

.PHONY: test doc all
