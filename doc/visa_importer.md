# Visa Importer

Importing data directly from network attached device works via the `io.read` method.

Currently there are the following devices implemented:

- Keysight oscilloscope (LAN)
- Rohde & Schwarz spectrum analyzer (LAN)
- Rohde & Schwarz oscilloscope (LAN)
- HP4395a (GPIB)

Feel free to add more devices.

```python
In  [0]: from openqlab import io

In  [1]: data = io.read("TCPIP::hostname_or_ip::INSTR")

In  [2]: data.head()
Out [2]:
------------------------------------------------------------
xincrement : 8.064e-07
xreference : 0
average_count : 1
xorigin : 0.00754
yUnit : V
points : 992
type : normal
xUnit : s
------------------------------------------------------------
                 1         2        3         4
Time
0.007540  0.000252 -0.004599 -1.09422  0.029460
0.007541 -0.000150 -0.003795 -1.09422 -0.010741
0.007542  0.000654 -0.003393 -1.09422 -0.010741
0.007542  0.000654 -0.003393 -1.29523  0.009359
0.007543  0.000654 -0.004197 -1.09422  0.009359
```

**Do not forget to save the data!**

```python
dc.to_csv("filename.csv")
```
