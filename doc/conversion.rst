:mod:`openqlab.conversion` -- Data Conversions
**********************************************

dB
--

Decibel conversions.

.. automodule:: openqlab.conversion.db
    :members:


Utils
-----

Some unit utils.

.. automodule:: openqlab.conversion.utils
    :members:


Wavelength
----------

Wavelength calucations.

.. automodule:: openqlab.conversion.wavelength
    :members:
