:mod:`openqlab.plots` -- Plotting of Data
*****************************************

Time-Domain Plots
-----------------

.. automodule:: openqlab.plots.time_domain
    :members:

Frequency-Domain Plots
----------------------

.. automodule:: openqlab.plots.frequency_domain
    :members:


Gaussian Beam Plots
-------------------

.. automodule:: openqlab.plots.gaussian_beam
    :members:
