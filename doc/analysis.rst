:mod:`openqlab.analysis` -- Data Analysis Tools and Helpers
***********************************************************

.. note::
    This documentation is unfortunately very much incomplete. The modules
    documented below are fairly recent, but there is a lot of interesting old
    code, especially for use with covariance matrix calculations. Please have
    a look at the source code or old scripts for usage examples -- and it
    would be nice if you could provide documentation afterwards.

Gaussian Beam
-------------

Fit beam data obtained from a beam analyzer to the gaussian beam model using non-linear least squares.

.. automodule:: openqlab.analysis.gaussian_beam
    :members:

Cavity
------

Cavity calculations

.. automodule:: openqlab.analysis.cavity
    :members:

Phase
-----

Phase calculations

.. automodule:: openqlab.analysis.phase
    :members:

Servo Design
------------

Toolkit for simulating and designing servo controllers.

.. automodule:: openqlab.analysis.servo_design
    :members:
