# Installation

## Prerequisites

`openqlab` requires a working `Python` installation on your PC. Currently, the easiest way to get this is to install the [Anaconda](https://www.anaconda.com/download/) distribution. Select the `Python 3.x` version (at the time of writing, x=7).

## Using the PyPi server (recommended)

This is the recommended, easiest version to install openqlab.
```bash
pip3 install --upgrade --user openqlab
```
If you don't have `pip3` on your system, try `pip`.
To install system wide don't use the `--user` flag.

## Manual installation on Linux

You can get the current version on our [deployment server](https://las-nq-serv.physnet.uni-hamburg.de/software-builds/nq-lab/) or on the [gitlab server](https://gitlab.aei.uni-hannover.de/las-nq/lab/).

Unpack the openqlab archive and `cd` to it.

### Install dependencies

For running the `setup.py` script you need to install the python `setuptools`:

```bash
sudo apt-get install python3-setuptools-scm
```

To install all pythonic dependencies you can simply run:

```bash
make requirements
```

### Install openqlab

Install `openqlab` for the current user:

```bash
python3 setup.py install --user
```

If you want to install it for all users on the PC, run:

```bash
sudo python3 setup.py install
```

## Development

If you want to take part in the development install the additional requirements from `requirements-devel.txt`:

```python
make requirements-devel

# If you want to install at user level:
make requirements-devel-user
```
