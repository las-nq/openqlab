# Installation

## Prerequisites

`openqlab` requires a working `Python` installation on your PC. Currently, the easiest way to get this is to install the [Anaconda](https://www.anaconda.com/download/) distribution. Select the `Python 3.x` version (at the time of writing, x=7).

## Using the PyPi server (recommended)

This is the recommended, easiest version to install openqlab.
```bash
pip3 install --upgrade --user openqlab
```
If you don't have `pip3` on your system, try `pip`.
To install system wide don't use the `--user` flag.

## Extras
### HDF support
To install `openqlab` with [HDF](https://de.wikipedia.org/wiki/Hierarchical_Data_Format) for `DataContainer` support use
```bash
pip3 install openqlab[hdf]
```

### VISA importers
To install `openqlab` with VISA importers use
```bash
pip3 install openqlab[visa]
```
