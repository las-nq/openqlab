:tocdepth: 2


Welcome to openqlab's documentation!
====================================

:mod:`openqlab` provides a collection of useful tools and helpers for the
analysis of lab data in the Nonlinear Quantum Optics Group at the University
of Hamburg.

Contents
========

.. toctree::
    :maxdepth: 2

    Installation <install>
    Usage <usage>

Reference
=========

.. toctree::
   :maxdepth: 2

   analysis
   conversion
   io
   plots
   datacontainer

Contribute
==========

Most of the original content in this package was written during the PhD theses of
Sebastian Steinlechner and Tobias Gehring. It is currently maintained by
Christian Darsow-Fromm and Jan Petermann and is looking for more
volunteers who would like to contribute.

If you want to contribute, feel free to do so. The source code is hosted on
https://gitlab.com/las-nq/openqlab

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
