Introduction
************

`openqlab` is conveniently used inside *Jupyter* notebooks. *Jupyter* is a part of e.g. the Anaconda installation. It allows to combine *Python* code together with images and documentation in a single, nicely formatted notebook page.



