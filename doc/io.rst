:mod:`openqlab.io` -- Import of Instrument Data
***********************************************

Convenient importers of instrument data.

Import Functions
----------------

.. automodule:: openqlab.io
    :members: read,import_data


Base Importer
-------------

.. automodule:: openqlab.io.base_importer
    :members:


Importers
---------

.. automodule:: openqlab.io.importers
    :members:
