# Contributing to openqlab
Thanks you for considering a contribution to `openqlab`. You can help to improve this software regardless of you programming experience! All contributions, bug reports, bug fixes, documentation improvements, enhancements and ideas are welcome. 

[[_TOC_]]

## Bug reports

If you find a bug, please create a new issue with the [bug report template](https://git.physnet.uni-hamburg.de/las-nq/openqlab/-/issues/new?issuable_template=Bug).
Describe the bug and attach relevant logs or screenshots if possible.

## Getting started
We use the label ~"Good first issue" for smaller issues that are well suited for beginners.
If you want to improve the documentation look for issues labeled with ~"Documentation".

When you want to work on an issue, please open a new merge request and mention one of the core developers to review you changes.

## Development
If you want to work on the code, please read through the following guidelines for a clean code base.

### Dependency Management
We use [poetry](https://python-poetry.org) for dependency management. To install poetry, please refer to their [documentation](https://python-poetry.org/docs/#installation).

 To install your development environment with all neccessary dependencies run
```sh
poetry install
```

### Testing
Please write unittests if you add new features.
The structure for the test should represent the structure of the package itself.
Each subpackage should have its own folder prefixed with `test_` and should contain subfolders with the same structure.
Every `.py` file (module) should be represented by one folder containing test files that test specific functions of that file.
For example:
- `tests`
    - `test_subpackage1`
        - `test_module1`
            - `test_function1_of_module1.py`
            - `test_function2_of_module1.py`
        - `test_module2`
            - `test_function1_of_module2.py`
            - `test_function2_of_module2.py`
    - `test_subpackage2`

For very simple classes or modules, the whole module can be tested in one `test_module.py` file but may still be contained inside a folder with the same name.
All tests located in the `src/tests/` directory are automatically tested when pushing to Gitlab.

To run them manually use:
```sh
poetry run py.test -x
```
or simply
```bash
make test
```

### Typing
We strongly encourage the use of [type hints](https://docs.python.org/3/library/typing.html) and use 
<a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/%20type_checker-mypy-%231674b1?style=flat"></a> as static type checker.

```sh
poetry run mypy src
```
or simply
```sh
make mypy
```

### Code Style
We use <a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg"></a>. Please have a look at their [documentation](https://black.readthedocs.io/en/stable/#) for details. 
You can format the code by running
```sh
poetry run black src
```
or simply
```sh
make black
```

### pre-commit
To make it simple to adhere to our development guidelines we recommend to use [pre-commit](https://pre-commit.com) to run the above checks before each commit to ensure you only commit clean code.
To use it, you only have to run 
```sh
poetry run pre-commit install
```
once for the project.


### Changelog

Please write every change that seems significant in the [CHANGELOG.md](CHANGELOG.md) file.

--------------------------------

## Release a new version

### 1. Use poetry to change the version number
```bash
poetry version patch  # small fixes
poetry version minor  # small features
poetry version major  # breaking changes
```

### 2. Edit the changelog

Change the headline of `[unreleased]` and use the version name.

### 3. Merge it to Master

From the `master` branch the deployment process will finish it automatically.
