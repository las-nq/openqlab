import glob
import importlib
import inspect
import os

from ..base_importer import BaseImporter
from . import utils
from .ascii import ASCII
from .data_container_csv import DataContainerCSV
from .hp4395a import HP4395A
from .keysight_binary import KeysightBinary
from .keysight_csv import KeysightCSV
from .keysight_fra import KeysightFRA
from .rhode_schwarz import RhodeSchwarz
from .tektronix import Tektronix
from .tektronix_dpx import TektronixDPX
from .tektronix_spectrogram import TektronixSpectrogram

# TODO maybe refactoring
IMPORTER_DIR = os.path.abspath(__file__)

IMPORTER_DIR = os.path.dirname(IMPORTER_DIR)

for fn in glob.glob(IMPORTER_DIR + "/*.py"):
    importer = utils.get_file_basename(fn)
    if importer in ("__init__", "utils"):
        continue
    try:
        importer_module = importlib.import_module(
            "openqlab.io.importers." + importer, package="openqlab.io.importers"
        )
        for m in inspect.getmembers(importer_module, inspect.isclass):
            name, klass = m
            if issubclass(klass, BaseImporter):
                globals()[name] = klass
    except ImportError as e:
        print(e)
        continue
