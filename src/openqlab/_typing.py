from typing import IO, Union
from os import PathLike

FilepathOrBuffer = Union[str, PathLike, IO]
