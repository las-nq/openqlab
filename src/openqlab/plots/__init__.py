from openqlab.plots.frequency_domain import (
    amplitude_phase,
    power_spectrum,
    relative_input_noise,
)
from openqlab.plots.gaussian_beam import beam_profile
from openqlab.plots.time_domain import scope, zero_span
