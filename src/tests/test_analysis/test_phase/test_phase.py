import logging as log
import unittest

from pandas import DataFrame

from openqlab import DataContainer, analysis, io


class TestPhase(unittest.TestCase):
    def test_accumulated_phase(self):
        df = DataFrame({"a": [0, 120, 180, -170, -110]})
        analysis.phase.accumulated_phase(df["a"])
        self.assertTrue(
            df.equals(DataFrame({"a": [0, 120, 180, -170 + 360, -110 + 360]}))
        )

        df = DataContainer({"a": [0, -120, -180, 170, 110]})
        analysis.phase.accumulated_phase(df["a"])
        self.assertTrue(
            df.equals(DataFrame({"a": [0, -120, -180, 170 - 360, 110 - 360]}))
        )

    def test_fail_on_multiple_columns(self):
        df = DataContainer(
            {"a": [0, -120, -180, 170, 110], "b": [0, -120, -180, 170, 110]}
        )
        with self.assertRaises((TypeError, ValueError)):
            analysis.phase.accumulated_phase(df)
