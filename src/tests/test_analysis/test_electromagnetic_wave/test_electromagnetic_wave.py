from pathlib import Path
from unittest import TestCase

import numpy as np

from openqlab.analysis.electromagnetic_wave import ElectromagneticWave, wien_law

filedir = Path(__file__).parent


class TestElectromagneticWave(TestCase):
    def setUp(self):
        self.wavelength = 1e-6
        self.EM = ElectromagneticWave(self.wavelength)

        self.wavelengths = np.array([1e-6, 2e-6])
        self.EMarr = ElectromagneticWave(self.wavelengths)

    def test___repr__(self):
        expected = "ElectromagneticWave(wavelength=1 µm)"
        result = repr(self.EM)
        self.assertEqual(expected, result)

        expected = "ElectromagneticWave(wavelength=1 µm)"
        result = repr(self.EMarr)
        self.assertEqual(expected, result)

    def test___str__(self):
        expected = "ElectromagneticWave(wavelength=1 µm)"
        result = f"{self.EM}"
        self.assertEqual(expected, result)

        expected = "ElectromagneticWave(wavelength=1 µm)"
        result = f"{self.EMarr}"
        self.assertEqual(expected, result)

    def test_wien_law(self):
        expected = 2.897771955e-3
        result = wien_law(1)
        self.assertAlmostEqual(expected, result)

        expected = 2.897771955e-3
        result = wien_law(np.array([1]))
        self.assertAlmostEqual(expected, result)

    def test_frequency(self):
        expected = 299792458000000.0
        self.assertEqual(expected, self.EM.f)

        expected = 299792458000000.0
        self.assertEqual(expected, self.EMarr.f[0])

    def test_wavenumber(self):
        expected = 1 / self.wavelength
        self.assertAlmostEqual(expected, self.EMarr.wavenumber[0])

    def test_omega(self):
        expected = 1883651567308853.2
        self.assertAlmostEqual(expected, self.EMarr.omega[0])

    def test_lambda0(self):
        expected = self.wavelength
        with self.assertWarns(DeprecationWarning):
            result = self.EM.lambda0
        self.assertEqual(expected, result)

        expected = self.wavelength
        with self.assertWarns(DeprecationWarning):
            result = self.EMarr.lambda0[0]
        self.assertEqual(expected, result)

    def test_eV(self):
        expected = 1.2398419843320025
        self.assertAlmostEqual(expected, self.EM.eV)

        self.assertAlmostEqual(expected, self.EMarr.eV[0])

    def test_temperature(self):
        expected = 2897.771955
        self.assertAlmostEqual(expected, self.EM.temperature)

        self.assertAlmostEqual(expected, self.EMarr.temperature[0])

    def test_ideal_responsivity(self):
        expected = 0.8065543937349212
        self.assertAlmostEqual(expected, self.EM.ideal_responsivity)

        self.assertAlmostEqual(expected, self.EMarr.ideal_responsivity[0])

    def test_quantum_efficiency(self):
        expected = 0.6199209921660013
        self.assertAlmostEqual(expected, self.EM.quantum_efficiency(0.5))

        self.assertAlmostEqual(expected, self.EMarr.quantum_efficiency(0.5)[0])

    def test_shotnoise(self):
        expected = 8.91391240062169e-10
        self.assertAlmostEqual(expected, self.EM.shotnoise(2))
        self.assertAlmostEqual(expected, self.EM.shotnoise(2.0))

        self.assertAlmostEqual(expected, self.EMarr.shotnoise(2)[0])
        self.assertAlmostEqual(expected, self.EMarr.shotnoise(2.0)[0])

    def test_RIN(self):
        expected = 4.456956200310845e-10
        self.assertAlmostEqual(expected, self.EM.RIN(2))
        self.assertAlmostEqual(expected, self.EM.RIN(2.0))

        self.assertAlmostEqual(expected, self.EMarr.RIN(2)[0])
        self.assertAlmostEqual(expected, self.EMarr.RIN(2.0)[0])

    def test_from_f(self):
        em = ElectromagneticWave.from_f(2)
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertEqual(149896229.0, em.wavelength)

        em = ElectromagneticWave.from_f(np.array([2]))
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertEqual(149896229.0, em.wavelength[0])

    def test_from_omega(self):
        em = ElectromagneticWave.from_omega(12.566370614359172)  # 2*2pi
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertEqual(149896229.0, em.wavelength)

        em = ElectromagneticWave.from_omega(np.array([12.566370614359172]))  # 2*2pi
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertEqual(149896229.0, em.wavelength[0])

    def test_from_wavenumber(self):
        em = ElectromagneticWave.from_wavenumber(10.0)  # 10/m
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertEqual(0.1, em.wavelength)

        em = ElectromagneticWave.from_wavenumber(np.array([10.0]))  # 10/m
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertEqual(0.1, em.wavelength[0])

    def test_from_eV(self):
        em = ElectromagneticWave.from_eV(1.2398419843320025)  # 10/m
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertAlmostEqual(1e-6, em.wavelength)

        em = ElectromagneticWave.from_eV(np.array([1.2398419843320025]))  # 10/m
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertAlmostEqual(1e-6, em.wavelength[0])

    def test_from_temperature(self):
        em = ElectromagneticWave.from_temperature(300.0)
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertAlmostEqual(9.65923985e-06, em.wavelength)

        em = ElectromagneticWave.from_temperature(np.array([300.0]))
        self.assertIsInstance(em, ElectromagneticWave)
        self.assertAlmostEqual(9.65923985e-06, em.wavelength[0])
