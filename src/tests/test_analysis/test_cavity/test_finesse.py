import unittest
from pathlib import Path

import matplotlib.pyplot as plt

from openqlab import io
from openqlab.analysis.cavity import finesse

filedir = Path(__file__).parent


class TestFinesse(unittest.TestCase):
    def setUp(self):
        self.data = io.read(f"{filedir}/opo_scan.csv", importer="KeysightCSV")
        plt.figure()

    def test_finesse_calculation(self):
        result = finesse(self.data[2])
        self.assertAlmostEqual(result[0], 54.27, places=1)
        self.assertAlmostEqual(result[1], 67.45, places=1)

    def test_finesse_plot(self):
        finesse(self.data[2], plot=True)

    def test_finesse_two_columns(self):
        with self.assertRaises((ValueError, TypeError)):
            finesse(self.data[[1, 2]])

    def test_finesse_flipping(self):
        data = io.read(f"{filedir}/cavities_refl_HighRes_20220627.csv")
        result = finesse(data["Ch 2 (V)"].loc[0:])

        self.assertAlmostEqual(result[0], 271, places=0)
        self.assertAlmostEqual(result[1], 265, places=-1)

    def _test_finesse_complain_about_too_many_maxima(self):
        data = io.read(f"{filedir}/cavities_refl_HighRes_20220627.csv")
        with self.assertRaises((ValueError, TypeError)):
            finesse(data["Ch 2 (V)"])

    def test_finesse_peak_finding(self):
        data = io.read(f"{filedir}/cavities_refl_20220628_HighRes.csv")
        result = finesse(data["Ch 1 (V)"].loc[0:5], plot=True)

        self.assertAlmostEqual(result[0], 289, places=0)
        self.assertAlmostEqual(result[1], 325, places=0)
