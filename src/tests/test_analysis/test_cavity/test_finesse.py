import unittest
from pathlib import Path

import matplotlib.pyplot as plt

from openqlab import io
from openqlab.analysis.cavity import finesse

filedir = Path(__file__).parent


class TestFinesse(unittest.TestCase):
    def setUp(self):
        self.data = io.read(f"{filedir}/opo_scan.csv", importer="KeysightCSV")
        plt.figure()

    def test_finesse_calculation(self):
        result = finesse(self.data["opo_scan_2"])
        self.assertAlmostEqual(result[0], 54.27, places=1)
        self.assertAlmostEqual(result[1], 67.45, places=1)

    def test_finesse_plot(self):
        finesse(self.data["opo_scan_2"], plot=True)

    def test_finesse_two_columns(self):
        with self.assertRaises(ValueError):
            finesse(self.data[["opo_scan_1", "opo_scan_2"]], plot=True)
