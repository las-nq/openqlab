from pathlib import Path
from unittest import TestCase

import matplotlib.pyplot as plt

from openqlab import io
from openqlab.analysis.cavity import linewidth

filedir = Path(__file__).parent


class TestLinewidth(TestCase):
    def setUp(self):
        base = f"{filedir}/"
        data = io.read(
            [base + "linewidth_err.csv", base + "linewidth_aux.csv"],
            importer="KeysightCSV",
        )
        data.columns = ["linewidth_err_1", "linewidth_aux_1"]
        self.err = data["linewidth_err_1"]
        self.aux = data["linewidth_aux_1"]

    def test_defaults(self):
        self.assertAlmostEqual(
            linewidth(self.err, self.aux, 49.6e6), 8.3e6, delta=0.1e6
        )

    def test_with_plot(self):
        self.assertAlmostEqual(
            linewidth(self.err, self.aux, 49.6e6, True), 8.3e6, delta=0.1e6
        )

    def test_with_positive_offset(self):
        self.err += 13
        self.aux += 17
        self.assertAlmostEqual(
            linewidth(self.err, self.aux, 49.6e6), 8.3e6, delta=0.1e6
        )

    def test_with_negative_offset(self):
        self.err -= 5e-3
        self.aux -= 1e-3

        self.assertAlmostEqual(
            linewidth(self.err, self.aux, 49.6e6), 8.3e6, delta=0.1e6
        )

    def test_with_negative_aux_sign(self):
        self.aux *= -1
        self.assertAlmostEqual(
            linewidth(self.err, self.aux, 49.6e6), 8.3e6, delta=0.1e6
        )

    def _test_with_negative_err_sign(self):
        self.err *= -1
        linewidth(self.err, self.aux, 49.6e6)
        plt.show()

        self.assertAlmostEqual(
            linewidth(self.err, self.aux, 49.6e6), 8.3e6, delta=0.1e6
        )

    def test_guessing_fwhm(self):
        self.assertAlmostEqual(
            linewidth(self.err, self.aux, 49.6e6, fwhm_guess=10e6), 8.3e6, delta=0.1e6
        )
