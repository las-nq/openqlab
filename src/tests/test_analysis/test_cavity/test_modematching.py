import unittest
from pathlib import Path

import matplotlib.pyplot as plt

from openqlab import io
from openqlab.analysis.cavity import modematching

filedir = Path(__file__).parent


class TestModematching(unittest.TestCase):
    def setUp(self):
        self.data = io.read(f"{filedir}/opo_scan.csv", importer="KeysightCSV")
        plt.figure()

    def test_default_inverted(self):
        result = modematching(self.data[2], plot=True)
        # plt.show()
        self.assertAlmostEqual(result, 0.955, places=2)

    def test_default_not_inverted(self):
        self.data *= -1
        result = modematching(self.data[2], plot=True)
        self.assertAlmostEqual(result, 0.955, places=2)

    def test_to_many_columns(self):
        with self.assertRaises((TypeError, ValueError)):
            modematching(self.data)

    def test_fail_with_only_one_peak(self):
        with self.assertRaises(ValueError) as err:
            modematching(self.data[2].loc[:0.01])
        self.assertEqual(
            err.exception.args[0],
            "The main mode must occur exactly two times for the algorithm to work, "
            "but it found 1 main modes.",
        )

    def test_with_much_offset(self):
        self.data *= -1
        self.data -= 2
        result = modematching(self.data[2], offset=-1.98, plot=True)
        self.assertAlmostEqual(result, 0.955, places=2)

    def test_deprecation(self):
        with self.assertWarns(DeprecationWarning):
            modematching(self.data[2], without_main_peaks=True)

    def test_cropped_main_peak(self):
        data = io.read(f"{filedir}/opo_scan_zoom.csv", importer="KeysightCSV")
        mm = modematching(
            data[2].loc[0.0015:0.0155],
            offset=-0.015,
            plot=True,
            U_max=9.27,
            rel_prominence=0.003,
        )
        self.assertAlmostEqual(mm, 0.92, places=2)
