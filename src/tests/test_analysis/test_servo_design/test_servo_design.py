import os
import unittest
from pathlib import Path

import jsonpickle
import matplotlib as mp
import numpy as np
from pandas import DataFrame

from openqlab import io
from openqlab.analysis.servo_design import (
    Differentiator,
    Filter,
    Integrator,
    Lowpass,
    Notch,
    ServoDesign,
)
from tests.test_plots.test_plots_tear_down import TestPlotTearDown

filedir = Path(__file__).parent


class TestFilter(unittest.TestCase):
    def test_integrator(self):
        corner_frequency = 840
        second_parameter = 5300
        i = Integrator(corner_frequency, second_parameter)

        np.testing.assert_allclose(i.zeros, -corner_frequency)
        np.testing.assert_allclose(i.poles, -second_parameter)
        np.testing.assert_allclose(i.gain, 1)

        i.second_parameter = corner_frequency * 0.001
        np.testing.assert_allclose(i.zeros, -corner_frequency)
        np.testing.assert_allclose(i.poles, -corner_frequency / 1000)

    def test_sF_getters_setters(self):
        for F in [Integrator, Differentiator]:
            f = F(1, 514)
            self.assertEqual(f.sF, 514)
            f.sF = 492
            self.assertEqual(f.sF, 492)

        f = Notch(1, 514)
        self.assertEqual(f.Q, 514)
        f.Q = 492
        self.assertEqual(f.Q, 492)

    def test_differentiator(self):
        corner_frequency = 840
        second_parameter = 5300
        i = Differentiator(corner_frequency, second_parameter)
        np.testing.assert_allclose(i.zeros, -corner_frequency)
        np.testing.assert_allclose(i.poles, -second_parameter)
        np.testing.assert_allclose(i.gain, 6.309524)

        i.second_parameter = None
        np.testing.assert_allclose(i.zeros, -corner_frequency)
        np.testing.assert_allclose(i.poles, -corner_frequency * 10)
        np.testing.assert_allclose(i.gain, 10)

    def test_lowpass(self):
        corner_frequency = 840
        second_parameter = 3
        i1 = Lowpass(corner_frequency, second_parameter)
        np.testing.assert_allclose(i1.zeros, -corner_frequency)
        np.testing.assert_allclose(
            i1.poles, [-140.0 + 828.25116963j, -140.0 - 828.25116963j]
        )
        np.testing.assert_allclose(i1.gain, 705600)

        i2 = Lowpass(corner_frequency)
        np.testing.assert_allclose(i2.zeros, [])
        np.testing.assert_allclose(
            i2.poles, [-594.05940594 + 593.8799729j, -594.05940594 - 593.8799729j]
        )
        np.testing.assert_allclose(i2.gain, 705600)

    def test_notch(self):
        corner_frequency = 840
        second_parameter = 1
        i1 = Notch(corner_frequency, second_parameter)
        np.testing.assert_allclose(i1.zeros, [0.0 + 840.0j, -0.0 - 840.0j])
        np.testing.assert_allclose(
            i1.poles, [-420.0 + 727.46133918j, -420.0 - 727.46133918j]
        )
        np.testing.assert_allclose(i1.gain, 1)

        second_parameter = 32
        i2 = Notch(corner_frequency, second_parameter)
        np.testing.assert_allclose(i2.zeros, [0.0 + 840.0j, -0.0 - 840.0j])
        np.testing.assert_allclose(
            i2.poles, [-13.125 + 839.89745468j, -13.125 - 839.89745468j]
        )
        np.testing.assert_allclose(i2.gain, 1)

    def test_enabled_setter(self):
        f = Integrator(4899)
        f.enabled = False
        with self.assertRaises(TypeError):
            f.enabled = "nein"
        with self.assertRaises(TypeError):
            f.enabled = 1

    def test_filter_enabled(self):
        for F in [Integrator, Differentiator, Lowpass, Notch]:
            f = F(500, enabled=False)
            self.assertFalse(f.enabled)
            f.enabled = True
            self.assertTrue(f.enabled)
            f.enabled = False
            self.assertFalse(f.enabled)
            f = F(500, enabled=True)
            self.assertTrue(f.enabled)

    def test_too_many_zeros_or_poles(self):
        z = np.array([1, 2])
        p = np.array([1, 2])

        def calculate_dummy():
            return z, p, 5

        corner_frequency = 840
        second_parameter = 5300
        i = Integrator(corner_frequency, second_parameter)
        i.calculate = calculate_dummy

        with self.assertRaises(ValueError):
            z = np.array([1, 2, 3])
            i.update()
        with self.assertRaises(ValueError):
            p = np.array([1, 2, 3])
            i.update()


class TestServoDesign(TestPlotTearDown):
    def setUp(self):
        if os.environ.get("DISPLAY"):
            self.display_available = True
        else:
            self.display_available = False
        self.sd = ServoDesign()

    def test_set_plant_none(self):
        self.sd.plant = None

    def test_len_empty(self):
        self.assertEqual(len(self.sd), 0)
        self.sd.integrator(200)
        self.assertEqual(len(self.sd), 1)
        self.sd.clear()
        self.assertEqual(len(self.sd), 0)

    def test_isempty(self):
        self.sd.differentiator(100)
        self.assertFalse(self.sd.is_empty())
        self.sd.clear()
        self.assertTrue(self.sd.is_empty())

    def test_filter_list(self):
        self.sd.integrator(340)
        self.sd.notch(239, 10)
        self.assertEqual(len(self.sd), 2)
        self.sd.clear()
        self.assertEqual(self.sd.filters, [None] * self.sd.MAX_FILTERS)

    def test_change_a_filter_inplace(self):
        self.sd.integrator(247, 20)
        self.assertEqual(self.sd.filters[0].corner_frequency, 247)
        self.sd.filters[0].corner_frequency = 103
        self.assertEqual(self.sd.filters[0].corner_frequency, 103)

    def test_add_too_many_filters(self):
        for i in range(1, 6):
            self.sd.lowpass(300 * i)
        self.assertEqual(len(self.sd), 5)
        with self.assertRaises(IndexError):
            self.sd.integrator(304)
        self.sd.add(Lowpass(500), override=True)
        self.assertEqual(self.sd.get(4).corner_frequency, Lowpass(500).corner_frequency)

        with self.assertRaises(IndexError):
            self.sd.add(Differentiator(299), index=self.sd.MAX_FILTERS + 1)
        with self.assertRaises(IndexError):
            self.sd._add_filter_on_index(
                Differentiator(299), index=self.sd.MAX_FILTERS + 1
            )

    def test_add_wrong_type(self):
        with self.assertRaises(TypeError):
            self.sd.add(499)

    def test_get_index(self):
        self.sd.integrator(50)
        self.assertEqual(self.sd.get(1), None)
        self.assertEqual(
            self.sd.get(0).corner_frequency, Integrator(50).corner_frequency
        )

        with self.assertRaises(IndexError):
            self.sd.get(-1)
        with self.assertRaises(IndexError):
            self.sd.get(self.sd.MAX_FILTERS)

    def test_remove_filter(self):
        self.sd.integrator(10)
        self.sd.integrator(11)
        self.sd.integrator(12)
        self.sd.integrator(13)
        self.sd.integrator(14)
        self.assertEqual(len(self.sd), 5)
        self.sd.remove(2)
        self.assertEqual(self.sd.get(2), None)
        self.assertEqual(len(self.sd), 4)

        with self.assertRaises(IndexError):
            self.sd.remove(-1)
        with self.assertRaises(IndexError):
            self.sd.remove(self.sd.MAX_FILTERS)

    def test_set_filter_none(self):
        self.sd.integrator(10)
        self.sd.integrator(11)
        self.sd.integrator(12)
        self.sd.integrator(13)
        self.sd.integrator(14)
        self.assertEqual(len(self.sd), 5)
        self.sd.filters[3] = None
        self.assertEqual(len(self.sd), 4)
        self.assertIsNone(self.sd.filters[3])

    def test_add_filter_on_index(self):
        self.sd.add(Integrator(13), index=3)
        self.assertIsNone(self.sd.filters[2])
        self.assertEqual(len(self.sd), 1)
        self.assertFalse(self.sd.is_empty())

    def test_add_filter_on_first_none_index(self):
        self.sd.integrator(10)
        self.sd.integrator(11)
        self.sd.integrator(12)
        self.sd.filters[1] = None
        self.sd.integrator(42)
        self.assertEqual(self.sd.filters[1].corner_frequency, 42)
        self.assertEqual(len(self.sd), 3)

    def test_add_on_wrong_index(self):
        with self.assertRaises(IndexError):
            self.sd.add(Differentiator(34), index=5)

    def test_change_gain(self):
        self.assertEqual(self.sd.gain, 1)
        self.sd.gain = 5
        self.assertEqual(self.sd.gain, 5)
        self.sd.gain = 9.3
        self.assertEqual(self.sd.gain, 9.3)

    def test_gain_db(self):
        self.sd.gain = 10
        self.sd.log_gain(6)
        self.assertAlmostEqual(self.sd.gain, 20, delta=0.05)
        self.sd.log_gain(-6)
        self.assertAlmostEqual(self.sd.gain, 10)

    def test_zpk(self):
        zpk_expected = (np.array([]), np.array([]), 1.0)
        zpk = self.sd.zpk()
        for i in range(3):
            np.testing.assert_allclose(zpk[i], zpk_expected[i])

        self.sd.integrator(500)
        self.sd.differentiator(1000, 1000 * 1000)
        self.sd.notch(900, second_parameter=200)
        zpk_expected = (
            np.array([-500.0, -1000.0, 0.0 + 900.0j, -0.0 - 900.0j]),
            np.array([-0.5, -1e6, -2.25e00 + 899.9971875j, -2.25e00 - 899.9971875j]),
            1000.0,
        )
        zpk = self.sd.zpk()
        for i in range(3):
            np.testing.assert_allclose(zpk[i], zpk_expected[i])

    def test_set_plant(self):
        with self.assertRaises(TypeError):
            self.sd.plant = 8

        columns = ["Gain (dB)", "Phase (deg)"]
        fra = io.read(f"{filedir}/fra_3.csv")
        self.sd.plant = fra
        self.assertListEqual(self.sd.plant.columns.tolist(), columns)

        fra2 = fra.copy()
        del fra2["Gain (dB)"]
        del fra2["Phase (deg)"]
        with self.assertRaises(Exception):
            self.sd.plant = fra2

    def test_simple_plot(self):
        if self.display_available:
            plt = self.sd.plot()
            self.assertIsInstance(plt, mp.figure.Figure)
            ax = plt.axes[0]
            self.assertLessEqual(ax.get_xlim()[1], 1e6)
            self.assertGreaterEqual(ax.get_xlim()[1], 1e5)
            self.sd.integrator(500)
            self.sd.integrator(500)
            self.sd.integrator(500)
            self.sd.integrator(500)
            self.sd.integrator(500)
            plt = self.sd.plot()
            self.assertIsInstance(plt, mp.figure.Figure)

    def test_kwargs_plot(self):
        if self.display_available:
            plot = self.sd.plot(color="black")

    def test_plot_with_frequencies(self):
        if self.display_available:
            plt = self.sd.plot(freq=np.logspace(1, 3, num=100))
            ax = plt.axes[0]
            self.assertLessEqual(ax.get_xlim()[1], 1e4)
            self.assertGreaterEqual(ax.get_xlim()[1], 1e3)

    def test_plot_with_plant(self):
        if self.display_available:
            self.sd.differentiator(390)
            self.sd.plant = io.read(f"{filedir}/fra_3.csv")
            plt = self.sd.plot()
            self.assertIsInstance(plt, mp.figure.Figure)
            ax = plt.axes[0]
            self.assertLessEqual(ax.get_xlim()[1], 3e4)
            self.assertGreaterEqual(ax.get_xlim()[1], 5e3)

    def test_get_dataframe(self):
        self.sd.notch(1e3, second_parameter=3)
        df = self.sd.plot(plot=False)
        self.assertIsInstance(df, DataFrame)
        columns = ["Servo A", "Servo P"]
        self.assertListEqual(df.columns.tolist(), columns)

        self.sd.plant = io.read(f"{filedir}/fra_3.csv")
        df = self.sd.plot(plot=False)
        self.assertIsInstance(df, DataFrame)
        columns = ["Servo A", "Servo P", "Servo+TF A", "Servo+TF P"]
        self.assertListEqual(df.columns.tolist(), columns)

    def test_description(self):
        self.sd.integrator(438)
        self.assertEqual(self.sd.filters[0].description, "Int 438 Hz, Slope 438 mHz")
        self.sd.filters[0].corner_frequency = 1200
        self.assertEqual(self.sd.filters[0].description, "Int 1.2 kHz, Slope 438 mHz")

        self.sd.differentiator(438)
        self.assertEqual(self.sd.filters[1].description, "Diff 438 Hz, Slope 4.38 kHz")
        self.sd.filters[1].corner_frequency = 1200
        self.assertEqual(self.sd.filters[1].description, "Diff 1.2 kHz, Slope 4.38 kHz")

        self.sd.lowpass(438)
        self.assertEqual(self.sd.filters[2].description, "LP2 438 Hz, Q=0.707")
        self.sd.filters[2].corner_frequency = 1200
        self.sd.filters[2].second_parameter = 12
        self.assertEqual(self.sd.filters[2].description, "LP2 1.2 kHz, Q=12")

        self.sd.notch(438, second_parameter=11)
        self.assertEqual(self.sd.filters[3].description, "Notch 438 Hz, Q=11")
        self.sd.filters[3].corner_frequency = 1200
        self.sd.filters[3].second_parameter = 1.3
        self.assertEqual(self.sd.filters[3].description, "Notch 1.2 kHz, Q=1.3")

        self.assertEqual(
            self.sd.filters[3].description, self.sd.filters[3].description_long
        )

    def test_discrete_form(self):
        self.sd.integrator(500)
        self.sd.notch(900, second_parameter=200)
        discrete_orig = {
            "sampling_frequency": 100000.0,
            "gain": 1.0,
            "filters": [
                {
                    "description": "Int 500 Hz, Slope 500 mHz",
                    "sos": np.array(
                        [1.0156933, -0.98427528, 0.0, 1.0, -0.99996858, 0.0]
                    ),
                    "enabled": True,
                    "index": 0,
                },
                {
                    "description": "Notch 900 Hz, Q=200",
                    "sos": np.array(
                        [0.99985872, -1.996521, 0.99985872, 1.0, -1.996521, 0.99971745]
                    ),
                    "enabled": True,
                    "index": 1,
                },
            ],
        }

        discrete = self.sd.discrete_form(sampling_frequency=100e3)
        self.assertEqual(
            discrete["sampling_frequency"], discrete_orig["sampling_frequency"]
        )
        self.assertEqual(discrete["gain"], discrete_orig["gain"])
        for f1, f2 in zip(discrete["filters"], discrete_orig["filters"]):
            self.assertEqual(f1["description"], f2["description"])
            self.assertEqual(f1["enabled"], f2["enabled"])
            self.assertEqual(f1["index"], f2["index"])
            np.testing.assert_allclose(f1["sos"], f2["sos"])

    def test_discrete_form_deprecation(self):
        with self.assertWarns(DeprecationWarning):
            self.sd.discrete_form(fs=100e3)

    def test_correct_latency(self):
        self.sd.plant = io.read(f"{filedir}/fra_3.csv")
        df = self.sd.plot(plot=False, correct_latency=False)
        df_corrected = self.sd.plot(plot=False, correct_latency=True)
        columns = ["Servo A", "Servo P", "Servo+TF A", "Servo+TF P"]
        self.assertListEqual(list(df.columns), columns)
        self.assertEqual(
            df.iloc[-1]["Servo+TF P"] + 360 * df.index[-1] / 200000,
            df_corrected.iloc[-1]["Servo+TF P"],
        )

    def test_with_disabled_filter(self):
        zpk_expected = (np.array([]), np.array([]), 1.0)
        zpk = self.sd.zpk()
        for i in range(3):
            np.testing.assert_allclose(zpk[i], zpk_expected[i])

        self.sd.integrator(500)
        self.sd.differentiator(1000, 1000 * 1000)
        self.sd.notch(900, second_parameter=200)
        self.sd.notch(1400, second_parameter=200, enabled=False)
        self.sd.differentiator(3000, enabled=False)
        zpk_expected = (
            np.array([-500.0, -1000.0, 0.0 + 900.0j, -0.0 - 900.0j]),
            np.array([-0.5, -1e6, -2.25e00 + 899.9971875j, -2.25e00 - 899.9971875j]),
            1000.0,
        )
        zpk = self.sd.zpk()
        for i in range(3):
            np.testing.assert_allclose(zpk[i], zpk_expected[i])

    def test_discrete_form_with_disabled_filter(self):
        self.sd.integrator(500)
        self.sd.notch(900, second_parameter=200)
        self.sd.integrator(500, enabled=False)
        self.sd.lowpass(5000, enabled=False)
        discrete_orig = {
            "sampling_frequency": 100000.0,
            "gain": 1.0,
            "filters": [
                {
                    "description": "Int 500 Hz, Slope 500 mHz",
                    "sos": np.array(
                        [1.0156933, -0.98427528, 0.0, 1.0, -0.99996858, 0.0]
                    ),
                    "enabled": True,
                    "index": 0,
                },
                {
                    "description": "Notch 900 Hz, Q=200",
                    "sos": np.array(
                        [0.99985872, -1.996521, 0.99985872, 1.0, -1.996521, 0.99971745]
                    ),
                    "enabled": True,
                    "index": 1,
                },
                {
                    "description": "Int 500 Hz, Slope 500 mHz",
                    "sos": np.array(
                        [1.0156933, -0.98427528, 0.0, 1.0, -0.99996858, 0.0]
                    ),
                    "enabled": False,
                    "index": 2,
                },
                {
                    "description": "LP2 5 kHz, Q=0.707",
                    "sos": np.array(
                        [
                            0.01975329,
                            0.03950658,
                            0.01975329,
                            1.0,
                            -1.5609758,
                            0.64130708,
                        ]
                    ),
                    "enabled": False,
                    "index": 3,
                },
            ],
        }

        discrete = self.sd.discrete_form(sampling_frequency=100e3)
        self.assertEqual(
            discrete["sampling_frequency"], discrete_orig["sampling_frequency"]
        )
        self.assertEqual(discrete["gain"], discrete_orig["gain"])
        for f1, f2 in zip(discrete["filters"], discrete_orig["filters"]):
            self.assertEqual(f1["description"], f2["description"])
            self.assertEqual(f1["enabled"], f2["enabled"])
            self.assertEqual(f1["index"], f2["index"])
            np.testing.assert_allclose(f1["sos"], f2["sos"])

    def test_jsonpickle(self):
        self.sd.notch(900, second_parameter=200)
        self.sd.integrator(500)
        self.sd.lowpass(5000)

        # Encode and decode without plant
        sdjson = jsonpickle.encode(self.sd)
        sd = jsonpickle.decode(sdjson)
        self.assertEqual(self.sd.__str__(), sd.__str__())

        # Encode and decode with plant
        fra = io.read(f"{filedir}/fra_3.csv")
        self.sd.plant = fra
        sdjson = jsonpickle.encode(self.sd)
        sd = jsonpickle.decode(sdjson)
        self.assertEqual(self.sd.__str__(), sd.__str__())
        self.assertIsInstance(sd.plant, io.DataContainer)
        self.assertEqual(len(self.sd.plant), len(sd.plant))


if __name__ == "__main__":
    unittest.main()
