from pathlib import Path
from unittest import TestCase

from openqlab import io
from openqlab.analysis.gaussian_beam import fit_beam_data

filedir = Path(__file__).parent


class TestFitBeamData(TestCase):
    def setUp(self):
        self.data = io.read(filedir / "data/data.txt")
        self.data.index *= 1e-2
        self.data *= 0.5e-6

    def test_fit_is_returning_correct_waist_size(self):
        result = fit_beam_data(self.data, wavelength=1550e-9, plot=True)
        # 0.00040907470625501426
        self.assertAlmostEqual(result["minor"].w0, 0.0004087165488871951, places=7)
        # 0.00041337615931208037
        self.assertAlmostEqual(result["major"].w0, 0.0004130909518475293, places=7)

    def test_fit_is_returning_correct_waist_position(self):
        result = fit_beam_data(self.data, wavelength=1550e-9, plot=True)
        # 0.03172060232176013
        self.assertAlmostEqual(result["minor"].z0, 0.031756002684627374, places=5)
        # 0.02869408832318352
        self.assertAlmostEqual(result["major"].z0, 0.028717148597017843, places=5)

    def test_fit_is_returning_correct_waist_size_error(self):
        result, errors = fit_beam_data(
            self.data, wavelength=1550e-9, plot=True, errors=True
        )
        self.assertAlmostEqual(
            errors["minor"][0], 4.640154301711167e-06, places=7
        )  # 0.0000047
        self.assertAlmostEqual(
            errors["major"][0], 4.740376841132247e-06, places=7
        )  # 0.0000047

    def test_fit_is_returning_correct_waist_position_error(self):
        _, errors = fit_beam_data(self.data, wavelength=1550e-9, plot=True, errors=True)
        self.assertAlmostEqual(
            errors["minor"][1], 0.006177933538121581, places=5
        )  # 0.00621
        self.assertAlmostEqual(
            errors["major"][1], 0.006396576345439704, places=5
        )  # 0.00642
