from pathlib import Path
from unittest import TestCase

import pandas as pd

from openqlab import io
from openqlab.analysis.gaussian_beam import fit_beam_data

filedir = Path(__file__).parent


class TestFitBeamData(TestCase):
    def setUp(self):
        self.data = io.read(filedir / "data/data.txt")
        self.data.index *= 1e-2
        self.data *= 0.5e-6
        self.result = fit_beam_data(self.data, wavelength=1550e-9, plot=False)

    def test_fit_is_returning_correct_waist_size(self):
        # 0.00040907470625501426
        self.assertAlmostEqual(
            self.result.loc["w0", "minor"], 0.0004087165488871951, places=7
        )
        # 0.00041337615931208037
        self.assertAlmostEqual(
            self.result.loc["w0", "major"], 0.0004130909518475293, places=7
        )

    def test_fit_is_returning_correct_waist_position(self):
        # 0.03172060232176013
        self.assertAlmostEqual(
            self.result.loc["z0", "minor"], 0.031756002684627374, places=5
        )
        # 0.02869408832318352
        self.assertAlmostEqual(
            self.result.loc["z0", "major"], 0.028717148597017843, places=5
        )

    def test_fit_is_returning_correct_waist_size_error(self):
        self.assertAlmostEqual(
            self.result.loc["w0_error", "minor"], 4.640154301711167e-06, places=7
        )  # 0.0000047
        self.assertAlmostEqual(
            self.result.loc["w0_error", "major"], 4.740376841132247e-06, places=7
        )  # 0.0000047

    def test_fit_is_returning_correct_waist_position_error(self):
        self.assertAlmostEqual(
            self.result.loc["z0_error", "minor"], 0.006177933538121581, places=5
        )  # 0.00621
        self.assertAlmostEqual(
            self.result.loc["z0_error", "major"], 0.006396576345439704, places=5
        )  # 0.00642

    def test_fit_is_setting_wavelength(self):
        result = fit_beam_data(self.data, wavelength=1550e-9, plot=False)
        assert result.header["wavelength"] == 1550e-9

    def test_dataframe(self):
        data = pd.DataFrame(self.data)
        result = fit_beam_data(data, wavelength=1550e-9, plot=False)
        assert result.header["wavelength"] == 1550e-9

        # 0.03172060232176013
        self.assertAlmostEqual(
            self.result.loc["z0", "minor"], 0.031756002684627374, places=5
        )
        # 0.02869408832318352
        self.assertAlmostEqual(
            self.result.loc["z0", "major"], 0.028717148597017843, places=5
        )
