from unittest import TestCase

import numpy as np

from openqlab.analysis.gaussian_beam import GaussianBeam


class TestGaussianBeam(TestCase):
    def test_non_default_lambda(self):
        beam = GaussianBeam(wavelength=1550e-9)
        self.assertEqual(beam.wavelength, 1550e-9)
        self.assertEqual(beam.propagate(1).wavelength, 1550e-9)

    def test_R_property(self):
        beam = GaussianBeam(q=3 + 1j)
        self.assertAlmostEqual(beam.R, 10 / 3)

    def test_R_property_infty(self):
        beam = GaussianBeam(q=0 + 1j)
        self.assertAlmostEqual(beam.R, np.infty)

    def test_waist(self):
        beam = GaussianBeam.from_waist(1e-3, 0)
        self.assertEqual(beam.w, 1e-3)

    def test_position(self):
        beam = GaussianBeam.from_waist(1e-3, 0)
        self.assertEqual(beam.z0, 0)

    def test_negative_wavelength_raises_error(self):
        with self.assertRaises(ValueError):
            beam = GaussianBeam(wavelength=-1e-9)

    def test_divergence(self):
        beam = GaussianBeam()
        self.assertAlmostEqual(beam.divergence, 582e-6)
