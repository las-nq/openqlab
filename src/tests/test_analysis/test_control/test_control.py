import unittest

import numpy as np

from openqlab.analysis import control


class TestZieglerNichols(unittest.TestCase):
    result = control.ziegler_nichols_method(5.5, 33)
    np.testing.assert_array_almost_equal(result, [3.3, 0.2, 13.6125])
