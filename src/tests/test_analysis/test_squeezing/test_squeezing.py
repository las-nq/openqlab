import sys
from importlib import reload
from unittest import TestCase

import numpy as np
from numpy.testing import assert_array_almost_equal
from uncertainties import ufloat, unumpy

from openqlab.analysis import squeezing
from openqlab.conversion import db


class TestSqueezing(TestCase):
    def setUp(self):
        sys.modules["uncertainties"] = None
        reload(squeezing)
        reload(db)
        self.assertFalse(squeezing.UNCERTAINTIES)

    def test_losses(self):
        self.assertAlmostEqual(squeezing.losses(-5, 20), 0.311, places=3)
        self.assertAlmostEqual(squeezing.losses(-10, 20), 0.092, places=3)
        self.assertAlmostEqual(squeezing.losses(-10, 10), 0, places=3)

        assert_array_almost_equal(
            squeezing.losses([-5, -10, -10], np.array([20, 20, 10])),
            [0.311, 0.092, 0],
            decimal=3,
        )

    def test_initial_squeezing(self):
        self.assertAlmostEqual(squeezing.initial(-10, 10), 10, places=3)
        self.assertAlmostEqual(squeezing.initial(-5, 20), 21.607, places=3)

        assert_array_almost_equal(
            squeezing.initial(np.array([-10, -5]), [10, 20]), [10, 21.607], decimal=3
        )

    def test_max_squeezing(self):
        self.assertAlmostEqual(squeezing.max(0.1, -100), -10)
        self.assertAlmostEqual(squeezing.max(0.01, -100), -20)
        self.assertAlmostEqual(squeezing.max(0.4, -100), -3.9794, places=4)
        self.assertAlmostEqual(squeezing.max(0.25, -100), -6.021, places=3)

        assert_array_almost_equal(
            squeezing.max([0.1, 0.01, 0.4, 0.25], -100),
            [-10, -20, -3.979, -6.021],
            decimal=3,
        )

    def test_max_squeezing_raises_error(self):
        for initial in [1e-9, 2.3, 40, 100]:
            with self.assertRaises(ValueError):
                squeezing.max(0.1, initial)

    def test_max_squeezing_with_antisqz(self):
        self.assertAlmostEqual(squeezing.max(0, -9.29, antisqz=True), [-9.29, 9.29])

        assert_array_almost_equal(
            squeezing.max(0.1, -15, antisqz=True), [-8.912, 14.558], decimal=3
        )
        assert_array_almost_equal(
            squeezing.max(0.3, -20, antisqz=True), [-5.129, 18.47], decimal=3
        )
        assert_array_almost_equal(
            squeezing.max(0.6, -25, antisqz=True), [-2.209, 21.041], decimal=3
        )


class TestSqueezingWithUncertainties(TestCase):
    def setUp(self):
        del sys.modules["uncertainties"]
        reload(squeezing)
        reload(db)
        self.assertTrue(squeezing.UNCERTAINTIES)
        self.assertTrue(db.UNCERTAINTIES)

    def test_initial_squeezing(self):
        self.assertAlmostEqual(
            squeezing.initial(ufloat(-10, 0.4), ufloat(10, 0.4)).nominal_value,
            10,
            places=3,
        )
        self.assertAlmostEqual(
            squeezing.initial(ufloat(-5, 0.4), ufloat(20, 0.4)).nominal_value,
            21.607,
            places=3,
        )

        assert_array_almost_equal(
            squeezing.initial(np.array([-10, -5]), [10, 20]), [10, 21.607], decimal=3
        )

    def test_losses(self):
        self.assertAlmostEqual(
            squeezing.losses(ufloat(-5, 0.5), ufloat(20, 0.7)).nominal_value,
            0.311,
            places=3,
        )

        result = squeezing.losses(
            np.array([ufloat(-5, 0.3), ufloat(-10, 0.3), ufloat(-10, 0.3)]),
            np.array([ufloat(20, 0.3), ufloat(20, 0.3), ufloat(10, 0.3)]),
        )

        for r in zip(result, [0.311, 0.092, 0]):
            self.assertAlmostEqual(r[0].nominal_value, r[1], places=3)

    def test_max_squeezing(self):
        self.assertAlmostEqual(
            squeezing.max(ufloat(0.1, 0.25), -100).nominal_value, -10
        )
        self.assertAlmostEqual(
            squeezing.max(ufloat(0.01, 0.25), -100).nominal_value, -20
        )
        self.assertAlmostEqual(
            squeezing.max(ufloat(0.4, 0.25), -100).nominal_value, -3.9794, places=4
        )
        self.assertAlmostEqual(
            squeezing.max(ufloat(0.25, 0.25), -100).nominal_value, -6.021, places=3
        )
