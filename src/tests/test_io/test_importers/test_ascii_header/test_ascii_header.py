import logging
import re
import unittest
from collections import OrderedDict
from pathlib import Path

from openqlab import io
from openqlab.io.data_container import DataContainer
from openqlab.io.importers.ascii_header import ASCII_Header
from openqlab.io.importers.utils import UnknownFileType

filedir = Path(__file__).parent
datadir = filedir / "../data_files"

log = logging.getLogger(__name__)
log.getChild("ascii_header")
log.setLevel(logging.DEBUG)

file_handler = logging.FileHandler(filedir / f"{__name__}.log", mode="w")

log.addHandler(file_handler)


# log.addHandler(logging.StreamHandler())


class TestASCII_Header(unittest.TestCase):
    importer = ASCII_Header
    files_path = datadir
    supported_files_path = datadir / "ASCII_Header"
    multiline_comment = re.compile(
        "This is a multiline comment.\s+It continues at the next line.\s+And another line\s+with newline."
    )

    def read_file(self, file):
        log.info(f"\n{file}")
        try:
            data = self.importer(file).read()
            self.assertTrue(isinstance(data, DataContainer))
            self.assertFalse(data.empty)
            # self.assertTrue(data._is_numeric_mixed_type)
            for column in data.columns:
                self.assertFalse(data[column].isna().values.all())
            return data
        except Exception as e:
            print(f"{file}")
            raise type(e)(f'"{file}" raised Exception: {e}') from e

    @classmethod
    def setUpClass(cls):
        cls.supported_files = list(cls.supported_files_path.glob("*"))
        cls.supported_files = [
            file for file in cls.supported_files if not file.stem.startswith(".")
        ]
        cls.test_files = list(cls.files_path.glob("*/*"))
        cls.unsupported_files = [
            file for file in cls.test_files if file not in cls.supported_files
        ]
        assert cls.supported_files
        assert cls.unsupported_files

    def test_supported_files(self):
        for file in self.supported_files:
            self.read_file(file)

    @unittest.skip("Importer cannot recognize unsupported files")
    def test_unsupported_files(self):
        for file in self.unsupported_files:
            try:
                with self.assertRaises(UnknownFileType):
                    self.read_file(file)
            except AssertionError as e:
                raise AssertionError(f"{file} did not raise {UnknownFileType}") from e

    def test_correct_rows_singletrace_header_hashtag(self):
        file = self.supported_files_path / "header_#.csv"
        data = self.read_file(file)

        # header
        self.assertEqual("5 V", data.header["voltage"])
        self.assertEqual("6 m/s", data.header["speed"])
        self.assertEqual("7", data.header["septab"])
        self.assertTrue(self.multiline_comment.match(data.header["comment"]))
        self.assertNotIn("Detector-unit", data.header)
        self.assertIsInstance(data.header, OrderedDict)

        # columns and index
        self.assertEqual(["values"], list(data.columns))
        self.assertEqual("time", data.index.name)

        # data
        self.assertAlmostEqual(+1.280000e002, data.index[0])
        self.assertAlmostEqual(+2.560000e002, data.index[1])
        self.assertAlmostEqual(+1.408000e003, data.index[10])  # line 19 in file
        self.assertAlmostEqual(+2.560000e003, data.index[-1])

        self.assertAlmostEqual(-4.078996e001, data.iloc[0, 0])
        self.assertAlmostEqual(-5.155923e001, data.iloc[1, 0])
        self.assertAlmostEqual(-7.461130e001, data.iloc[10, 0])  # line 19 in file)
        self.assertAlmostEqual(-7.951345e001, data.iloc[-1, 0])

    def test_correct_rows_singletrace_header_hashtag_comma(self):
        file = self.supported_files_path / "header_#_comma.csv"
        data = self.read_file(file)

        # header
        self.assertEqual("5 V", data.header["voltage"])
        self.assertEqual("6 m/s", data.header["speed"])
        self.assertEqual("7", data.header["septab"])
        self.assertTrue(self.multiline_comment.match(data.header["comment"]))
        self.assertNotIn("Detector-unit", data.header)

        # columns and index
        self.assertEqual(["values"], list(data.columns))
        self.assertEqual("time", data.index.name)

        # data
        self.assertAlmostEqual(+1.280000e002, data.index[0])
        self.assertAlmostEqual(+2.560000e002, data.index[1])
        self.assertAlmostEqual(+1.408000e003, data.index[10])  # line 19 in file
        self.assertAlmostEqual(+2.560000e003, data.index[-1])

        self.assertAlmostEqual(-4.078996e001, data.iloc[0, 0])
        self.assertAlmostEqual(-5.155923e001, data.iloc[1, 0])
        self.assertAlmostEqual(-7.461130e001, data.iloc[10, 0])  # line 19 in file)
        self.assertAlmostEqual(-7.951345e001, data.iloc[-1, 0])

    def test_correct_rows_singletrace_header_dollar(self):
        file = self.supported_files_path / "header_$.csv"
        data = self.read_file(file)

        # header
        self.assertEqual("5 V", data.header["voltage"])
        self.assertEqual("6 m/s", data.header["speed"])
        self.assertEqual("7", data.header["septab"])
        self.assertTrue(self.multiline_comment.match(data.header["comment"]))
        self.assertNotIn("Detector-unit", data.header)

        # columns and index
        self.assertEqual(["values"], list(data.columns))
        self.assertEqual("time", data.index.name)

        # data
        self.assertAlmostEqual(+1.280000e002, data.index[0])
        self.assertAlmostEqual(+2.560000e002, data.index[1])
        self.assertAlmostEqual(+1.408000e003, data.index[10])  # line 19 in file
        self.assertAlmostEqual(+2.560000e003, data.index[-1])

        self.assertAlmostEqual(-4.078996e001, data.iloc[0, 0])
        self.assertAlmostEqual(-5.155923e001, data.iloc[1, 0])
        self.assertAlmostEqual(-7.461130e001, data.iloc[10, 0])  # line 19 in file)
        self.assertAlmostEqual(-7.951345e001, data.iloc[-1, 0])

    def test_correct_rows_singletrace_header_percent(self):
        file = self.supported_files_path / "header_%.csv"
        data = self.read_file(file)

        # header
        self.assertEqual("5 V", data.header["voltage"])
        self.assertEqual("6 m/s", data.header["speed"])
        self.assertEqual("7", data.header["septab"])
        self.assertTrue(self.multiline_comment.match(data.header["comment"]))
        self.assertNotIn("Detector-unit", data.header)

        # columns and index
        self.assertEqual(["values"], list(data.columns))
        self.assertEqual("time", data.index.name)

        # data
        self.assertAlmostEqual(+1.280000e002, data.index[0])
        self.assertAlmostEqual(+2.560000e002, data.index[1])
        self.assertAlmostEqual(+1.408000e003, data.index[10])  # line 19 in file
        self.assertAlmostEqual(+2.560000e003, data.index[-1])

        self.assertAlmostEqual(-4.078996e001, data.iloc[0, 0])
        self.assertAlmostEqual(-5.155923e001, data.iloc[1, 0])
        self.assertAlmostEqual(-7.461130e001, data.iloc[10, 0])  # line 19 in file)
        self.assertAlmostEqual(-7.951345e001, data.iloc[-1, 0])

    def test_ascii_import_with_io_read(self):
        file = datadir / "ASCII/dig0.TXT"
        data = io.read(file, importer="ASCII")
        # columns and index
        self.assertEqual(["dig0"], list(data.columns))
        self.assertEqual("x", data.index.name)
        self.assertEqual(data.index[0], 1.280000e2)
        self.assertEqual(data.iloc[0, 0], -4.078996e1)
        self.assertEqual(data.index[799], 1.024000e5)
        self.assertEqual(data.iloc[799, 0], -9.785766e1)

    def test_column_names_no_header(self):
        file = filedir / "column_names_no_header"
        data = io.read(file)
        log.debug("%s", data)

        # columns and index
        self.assertEqual(["amplitude"], list(data.columns))
        self.assertEqual("frequency", data.index.name)

    def test_no_column_names_header(self):
        file = filedir / "no_column_names_header"
        data = io.read(file)
        log.debug("%s", data)

        expected_header = {
            "comment": "this is a comment",
            "temperature": "20",
            "humidity": "30",
        }

        # columns and index
        self.assertEqual(expected_header, data.header)
        self.assertEqual([1], list(data.columns))
        self.assertEqual(0, data.index.name)

    def test_column_names_header(self):
        file = filedir / "column_names_header"
        data = io.read(file)
        log.debug("%s", data)

        expected_header = {
            "comment": "this is a comment",
            "temperature": "20",
            "humidity": "30",
        }

        # columns and index
        self.assertEqual(expected_header, data.header)
        self.assertEqual(["amplitude"], list(data.columns))
        self.assertEqual("frequency", data.index.name)

    def test_header_more_columns(self):
        file = self.supported_files_path / "header_#_more_columns.csv"
        data = self.read_file(file)

        # columns
        self.assertEqual("time", data.index.name)
        self.assertEqual(["values1", "values2"], list(data.columns))

        # data
        self.assertAlmostEqual(+1.280000e002, data.index[0])
        self.assertAlmostEqual(+2.560000e003, data.index[-1])

        self.assertAlmostEqual(-5.078996e001, data.iloc[0, 1])
        self.assertAlmostEqual(-9.951345e001, data.iloc[-1, 1])

    def test_more_columns_with_names(self):
        file = self.supported_files_path / "more_columns_with_names.csv"
        data = self.read_file(file)

        # columns
        self.assertEqual("time", data.index.name)
        self.assertEqual(["values1", "values2"], list(data.columns))

        # data
        self.assertAlmostEqual(+1.280000e002, data.index[0])
        self.assertAlmostEqual(+2.560000e003, data.index[-1])

        self.assertAlmostEqual(-5.078996e001, data.iloc[0, 1])
        self.assertAlmostEqual(-9.951345e001, data.iloc[-1, 1])

    def test_more_columns_without_names(self):
        file = self.supported_files_path / "more_columns_without_names.csv"
        data = self.read_file(file)

        # columns
        self.assertEqual(0, data.index.name)
        self.assertEqual(
            [1, 2],
            list(data.columns),
        )

        # data
        self.assertAlmostEqual(+1.280000e002, data.index[0])
        self.assertAlmostEqual(+2.560000e003, data.index[-1])

        self.assertAlmostEqual(-5.078996e001, data.iloc[0, 1])
        self.assertAlmostEqual(-9.951345e001, data.iloc[-1, 1])

    def test_commented_data_fields(self):
        file = self.supported_files_path / "header_$_comment.csv"
        data = self.read_file(file)

        self.assertAlmostEqual(+7.680000e002, data.index[5])
        self.assertAlmostEqual(+1.024000e003, data.index[6])
        self.assertAlmostEqual(+2.432000e003, data.index[-1])
        self.assertEqual(len(data), 17)

    def test_handwritten_file(self):
        file = self.supported_files_path / "fsr_shift.csv"
        data = self.read_file(file)

        assert (data.dtypes == [float]).all
        assert len(data) == 7
        assert data.index.name == "temperature (°C)"
        assert data.columns == ["FSR"]
        assert (
            data["FSR"].values
            == [
                0.0,
                0.5,
                1.0,
                2.0,
                3.0,
                4.0,
                5.0,
            ]
        ).all()
        assert (
            data.index.values
            == [
                41.061,
                40.99,
                40.903,
                40.753,
                40.617,
                40.461,
                40.319,
            ]
        ).all()

    def test_handwritten_file_with_comments(self):
        file = self.supported_files_path / "fsr_shift_comments.csv"
        data = self.read_file(file)

        expected_header = {
            "test_key": "15",
            "test_temp": "17 (°C)",
            "test_hum": "30 %",
            "comment": "this is a multiline\ncomment",
        }
        assert data.header == expected_header
        assert data.header
        assert (data.dtypes == [float]).all
        assert len(data) == 7
        assert data.index.name == "temperature (°C)"
        assert data.columns == ["FSR"]
        assert (
            data["FSR"].values
            == [
                0.0,
                0.5,
                1.0,
                2.0,
                3.0,
                4.0,
                5.0,
            ]
        ).all()
        assert (
            data.index.values
            == [
                41.061,
                40.99,
                40.903,
                40.753,
                40.617,
                40.461,
                40.319,
            ]
        ).all()
