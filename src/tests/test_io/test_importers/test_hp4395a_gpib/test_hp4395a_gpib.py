import platform
import unittest
from pathlib import Path

from openqlab.io.importers.hp4395a_gpib import HP4395A_GPIB
from openqlab.io.importers.utils import ImportFailed, UnknownFileType

filedir = Path(__file__).parent


class TestHP4395aGpib(unittest.TestCase):
    def setUp(self):
        # mock = MockVisa()
        # self.importer = HP4395A_GPIB("TCPIP::mockaddress", inst=mock)
        self.importer = HP4395A_GPIB("/dev/ttyUSB0::17")

    @unittest.skip("Only working with real device, yet.")
    def test_read(self):
        data = self.importer.read()
        print(data)
