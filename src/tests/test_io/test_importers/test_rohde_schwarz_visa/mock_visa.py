import gzip
from pathlib import Path
from re import match
from typing import List

filedir = Path(__file__).parent


class MockVisa:
    def __init__(self):
        self.frequency_start: str = "450000"
        self.frequency_stop: str = "3550000"
        self.frequency_span: str = "3100000"
        self.frequency_center: str = "2000000"
        self.frequency_rbw: str = "30000"
        self.frequency_vbw: str = "10000000"

        self.yUnit: str = "DBM"
        self.sweep_time: str = ".001"
        self.sweep_points: str = "30001"

        self.log = []
        self.read_termination = ""
        self.traces_enabled: List[str] = ["1", "0", "0"]
        self.trace_data = {}
        for i in range(1, 4):
            with gzip.open(filedir / f"col{i}.txt.gz", "rt") as f:
                self.trace_data[i] = f.read().strip()

        self.idn = "Rohde&Schwarz,FSU-3,200061/003,4.71"

    def open_resource(self, _: str):
        return self

    def write(self, command: str):
        known_commands: List[str] = []

        self.log.append(f"write: {command}")

        commands: list = command.lower().strip().split(" ")

        if commands[0] == "sweep:points":
            self.sweep_points = commands[1]
        elif commands[0] in known_commands:
            pass
        else:
            raise ValueError(f"Unknown command: {command}")

    def query(self, query: str):
        self.log.append(f"query: {query}")

        query = query.lower().strip()

        if query == "*idn?":
            return self.idn

        if query == "sense:frequency:center?; span?; start?; stop?":
            return ";".join(
                [
                    self.frequency_center,
                    self.frequency_span,
                    self.frequency_start,
                    self.frequency_stop,
                ]
            )

        if query == "unit:power?":
            return self.yUnit

        if query == "sweep:time?":
            return self.sweep_time

        if query == "sweep:points?":
            return self.sweep_points

        if query == "band:res?; vid?":
            return ";".join([self.frequency_rbw, self.frequency_vbw])

        m = match(r"disp:wind:trace(.):stat?", query)
        if m is not None:
            chan = int(m.group(1))
            return self.traces_enabled[chan - 1]

        m = match(r"trace\? trace(.)", query)
        if m is not None:
            chan = int(m.group(1))
            print(self.sweep_points)
            data = ",".join(self.trace_data[chan].split(",")[: int(self.sweep_points)])
            return data

        raise ValueError(f"Unknown query: {query}")
