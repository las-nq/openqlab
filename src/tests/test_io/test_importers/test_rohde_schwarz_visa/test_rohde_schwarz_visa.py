import platform
import unittest
from pathlib import Path

import openqlab.io.base_importer
from openqlab import io
from openqlab.io.importers.rohde_schwarz_visa import RohdeSchwarzVisa
from openqlab.io.importers.utils import ImportFailed, UnknownFileType

from .mock_visa import MockVisa

filedir = Path(__file__).parent


class TestRohdeSchwarzVisa(unittest.TestCase):
    def setUp(self):
        mock = MockVisa()
        self.importer = RohdeSchwarzVisa("TCPIP::mockaddress", inst=mock)

    def test_idn(self):
        self.assertEqual(self.importer.idn, "Rohde&Schwarz,FSU-3,200061/003,4.71")

    def test_fail_without_pyvisa(self):
        # TODO not the nicest way.
        # Change to proper mocking, if you know how it works exactly in this case.
        pyvisa_tmp = openqlab.io.base_importer.pyvisa
        openqlab.io.base_importer.pyvisa = None
        assert openqlab.io.base_importer.pyvisa is None

        with self.assertLogs(level="ERROR"):
            try:
                io.read("TCPIP::mockaddress", inst=MockVisa())
            except io.UndefinedImporter:
                pass

        openqlab.io.base_importer.pyvisa = pyvisa_tmp
        assert openqlab.io.base_importer.pyvisa is not None

    def test_read_data_frequency(self):
        self.importer._inst.traces_enabled = ["1"] * 3
        dc = self.importer.read()
        self.assertEqual(len(dc.columns), 3)
        self.assertEqual(dc.index.name, "Frequency")
        self.assertEqual(len(dc), 10000)
        self.assertEqual(dc.header["xUnit"], "Hz")

    def test_wrong_number_of_datapoints(self):
        mock = MockVisa()
        for n in [0, 30002, 65000, -1, -230]:
            with self.assertRaises(ValueError):
                self.importer = RohdeSchwarzVisa(
                    "TCPIP::mockaddress", inst=mock, data_points=n
                )

    def test_data_points_io_read(self):
        data = io.read("TCPIP::mockaddress", inst=MockVisa(), data_points=142)
        self.assertEqual(len(data), 142)

    def test_set_data_points(self):
        mock = MockVisa()
        self.importer = RohdeSchwarzVisa(
            "TCPIP::mockaddress", inst=mock, data_points=294
        )
        self.importer._inst.traces_enabled = ["1"] * 3
        dc = self.importer.read()
        self.assertEqual(self.importer._inst.sweep_points, "294")
        self.assertEqual(len(dc), 294)

    def test_read_data_zero_span(self):
        self.importer._inst.traces_enabled = ["1"] * 3
        self.importer._inst.frequency_span = "0"
        dc = self.importer.read()
        self.assertEqual(len(dc), 10000)
        self.assertEqual(dc.header["xUnit"], "s")
        self.assertEqual(dc.index.name, "Time")
        self.assertEqual(dc.index[0], 0)
        self.assertAlmostEqual(dc.index[-1], 0.001)

    def test_accept_multiple_addresses(self):
        mock = MockVisa()
        # Accept addresses
        for i in [1, 7, 0, 9, ""]:
            RohdeSchwarzVisa(f"TCPIP{i}::mockaddress", inst=mock)
        # Do not accept addresses
        for i in ["x", 10, 20, "ay"]:
            with self.assertRaises(UnknownFileType):
                RohdeSchwarzVisa(f"TCPIP{i}::mockaddress", inst=mock)

    def test_missing_data(self):
        self.importer._inst.traces_enabled[0] = "1"
        self.importer._inst.trace_data[1] = ""
        with self.assertRaises(ImportFailed):
            _ = self.importer.read()

    def test_wrong_data(self):
        self.importer._inst.traces_enabled[0] = "1"
        self.importer._inst.trace_data[1] = "031243"
        with self.assertRaises(ImportFailed):
            _ = self.importer.read()

    def test_without_active_trace(self):
        self.importer._inst.traces_enabled[0] = "0"
        with self.assertRaises(ImportFailed):
            _ = self.importer.read()

    def test_clipped_data(self):
        self.importer._inst.traces_enabled[0] = "1"
        self.importer._inst.trace_data[
            1
        ] = "1.35683e-002,-1.19603e-002,-3.11608e-003, 6.33216e-003, 9.14623e-003, 7.13618e-003, 1.03523e-002, 3.11608e-003, 5.93015e-003, 1.19603e-002"
        with self.assertRaises(ImportFailed):
            _ = self.importer.read()

    @unittest.skip("bla")
    def test_data_values(self):
        self.importer._inst.traces_enabled = ["1"] * 3
        dc = self.importer.read()
        print(dc)
        self.assertEqual(dc[1].iloc[0], 1.35683e-2)
        self.assertEqual(dc[2].iloc[0], 2.35683e-2)
        self.assertEqual(dc[3].iloc[0], 3.35683e-2)

        self.assertEqual(dc[1].iloc[1], -1.19603e-2)
        self.assertEqual(dc[2].iloc[1], -2.19603e-2)
        self.assertEqual(dc[3].iloc[1], -3.19603e-2)

        self.assertEqual(dc[1].iloc[-1], 1.19603e-2)
        self.assertEqual(dc[2].iloc[-1], 2.19603e-2)
        self.assertEqual(dc[3].iloc[-1], 3.19603e-2)

    @unittest.skipIf(
        platform.system() == "Windows", "Windows throws Error that you cannot import"
    )
    def test_open_real_resource(self):
        with self.assertRaises((ConnectionRefusedError, BrokenPipeError)):
            self.importer = RohdeSchwarzVisa("TCPIP::localhost::INSTR")

    def test_wrong_device_idn(self):
        mock = MockVisa()
        importer = RohdeSchwarzVisa("TCPIP::mockaddress", inst=mock)
        importer._inst.idn = "Something wrong"
        with self.assertRaises(UnknownFileType):
            importer._check_connection()
