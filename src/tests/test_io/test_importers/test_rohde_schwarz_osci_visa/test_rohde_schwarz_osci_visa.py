import platform
import unittest
from pathlib import Path

from openqlab.io.importers.rohde_schwarz_osci_visa import RohdeSchwarzOsciVisa
from openqlab.io.importers.utils import ImportFailed, UnknownFileType

from .mock_visa import MockVisa

filedir = Path(__file__).parent


class TestRohdeSchwarzOsciVisa(unittest.TestCase):
    def setUp(self):
        mock = MockVisa()
        self.importer = RohdeSchwarzOsciVisa("TCPIP::mockaddress", inst=mock)

    def test_idn(self):
        self.assertEqual(
            self.importer.idn,
            "Rohde&Schwarz,RTB2004,1333.1005k04/109983,02.202",
        )

    def test_without_scope_stopping(self):
        mock = MockVisa()
        importer = RohdeSchwarzOsciVisa(
            "TCPIP::mockaddress", inst=mock, stop_scope=False
        )
        _ = importer.read()
        self.assertNotIn("write: :RUN", importer._inst.log)
        self.assertNotIn("write: :STOP", importer._inst.log)

    def test_individual_number_of_points(self):
        mock = MockVisa()
        importer = RohdeSchwarzOsciVisa(
            "TCPIP::mockaddress", inst=mock, number_of_points="dmax"
        )
        self.assertIn("write: chan:data:poin dmax", importer._inst.log)

    def test_not_writing_number_of_points(self):
        mock = MockVisa()
        importer = RohdeSchwarzOsciVisa("TCPIP::mockaddress", inst=mock)
        for s in ["dmax", "max", "default"]:
            self.assertNotIn(f"write: chan:data:poin {s}", importer._inst.log)

    def test_read_data(self):
        dc = self.importer.read()
        self.assertEqual(len(dc.columns), 4)
        self.assertEqual(dc.index.name, "Time")

        self.assertIn("write: :RUN", self.importer._inst.log)
        self.assertIn("write: :STOP", self.importer._inst.log)

    def test_index(self):
        dc = self.importer.read()
        self.assertAlmostEqual(dc.index[5], 6.67e-5)
        self.assertAlmostEqual(dc.index[0], -6e-4)
        self.assertAlmostEqual(dc.index[-1], 6e-4)

    def test_missing_data(self):
        self.importer._inst.state["chan1:data?"] = ""
        with self.assertRaises(ImportFailed):
            dc = self.importer.read()

    def test_wrong_data(self):
        self.importer._inst.state["chan1:data?"] = "031243"
        with self.assertRaises(ImportFailed):
            dc = self.importer.read()

    def test_without_active_trace(self):
        for i in range(1, 5):
            self.importer._inst.state[f"chan{i}:state?"] = "0"
        with self.assertRaises(ImportFailed):
            dc = self.importer.read()
            self.assertEqual(self.importer._inst.state["chan1:state?"], "0")

    def test_clipped_data(self):
        self.importer._inst.state[
            "chan1:data?"
        ] = "1.35683e-002,-1.19603e-002,-3.11608e-003, 6.33216e-003, 9.14623e-003, 7.13618e-003, 1.03523e-002, 3.11608e-003, 1.19603e-002"
        with self.assertRaises(ImportFailed):
            dc = self.importer.read()

    def test_data_values(self):
        dc = self.importer.read()
        print(dc)
        self.assertEqual(dc[1].iloc[0], 1.35683e-2)
        self.assertEqual(dc[2].iloc[0], 2.35683e-2)
        self.assertEqual(dc[3].iloc[0], 3.35683e-2)
        self.assertEqual(dc[4].iloc[0], 4.35683e-2)

        self.assertEqual(dc[1].iloc[1], -1.19603e-2)
        self.assertEqual(dc[2].iloc[1], -2.19603e-2)
        self.assertEqual(dc[3].iloc[1], -3.19603e-2)
        self.assertEqual(dc[4].iloc[1], -4.19603e-2)

        self.assertEqual(dc[1].iloc[-1], 1.19603e-2)
        self.assertEqual(dc[2].iloc[-1], 2.19603e-2)
        self.assertEqual(dc[3].iloc[-1], 3.19603e-2)
        self.assertEqual(dc[4].iloc[-1], 4.19603e-2)

    @unittest.skipIf(
        platform.system() == "Windows", "Windows throws Error that you cannot import"
    )
    def test_open_real_resource(self):
        with self.assertRaises((ConnectionRefusedError, BrokenPipeError)):
            self.importer = RohdeSchwarzOsciVisa("TCPIP::localhost::INSTR")

    def test_wrong_device_idn(self):
        mock = MockVisa()
        importer = RohdeSchwarzOsciVisa("TCPIP::mockaddress", inst=mock)
        importer._inst.state["*idn?"] = "Something wrong"
        with self.assertRaises(UnknownFileType):
            importer._check_connection()
