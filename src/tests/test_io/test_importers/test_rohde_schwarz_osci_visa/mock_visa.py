from re import match
from typing import List


class MockVisa:
    def __init__(self):
        self.log = []
        self.read_termination = ""

        self.state = {
            "syst:err:all?": '0,"No error" ',
            "*idn?": "Rohde&Schwarz,RTB2004,1333.1005k04/109983,02.202",
            "chan:data:head?": "-6.00000E-04,6.00000E-04,10,1 ",
        }

        default_settings = {
            "bandwidth": "FULL",
            "coupling": "DCL",
            "label": "C1",
            "offset": "0.00E+00 ",
            "polarity": "NORM",
            "position": "0.00E+00 ",
            "range": "1.00E-02",
            "scale": "1.00E-03",
            "skew": "0.00E+00",
            "state": "1",
            "threshold": "5.00E-01 ",
            "type": "HRES",
            "zoffset": "0.00E+00",
        }
        for i in range(1, 5):
            for setting in default_settings:
                self.state[f"chan{i}:{setting}?"] = default_settings[setting]

        self.state.update(
            {
                "chan1:data?": "1.35683E-02,-1.19603E-02,-3.11608E-03,6.33216E-03,9.14623E-03,7.13618E-03,1.03523E-02,3.11608E-03,5.93015E-03,1.19603E-02",
                "chan2:data?": "2.35683E-02,-2.19603E-02,-4.11608E-03,7.33216E-03,9.14623E-03,7.13618E-03,1.03523E-02,3.11608E-03,5.93015E-03,2.19603E-02",
                "chan3:data?": "3.35683E-02,-3.19603E-02,-5.11608E-03,6.33216E-03,9.14623E-03,7.13618E-03,1.03523E-02,3.11608E-03,5.93015E-03,3.19603E-02",
                "chan4:data?": "4.35683E-02,-4.19603E-02,-7.11608E-03,6.33216E-03,9.14623E-03,7.13618E-03,1.03523E-02,3.11608E-03,5.93015E-03,4.19603E-02",
            }
        )

    def open_resource(self, _: str):
        return self

    def write(self, command: str):
        known_commands = {
            ":run": None,
            ":stop": None,
            "chan:type": ["samp", "hres"],
            "form": "ascii",
            "chan:data:poin": ["dmax", "default", "max"],
        }

        self.log.append(f"write: {command}")
        print(self.log)

        commands: list = command.lower().strip().split(" ")

        cmd = commands[0]

        assert cmd in known_commands, commands

        if len(commands) == 2:
            value = commands[1]
            assert value in known_commands[cmd]  # type:ignore

        assert len(commands) < 3

    def query(self, query: str):
        self.log.append(f"query: {query}")
        print(self.log)

        query = query.lower().strip()

        if query in self.state:
            return self.state[query]

        raise ValueError(f"Unknown query: {query}")
