"""
These tests verify the correct behaviour with the new inheritance mechanism of pandas from v1.2.
"""

import pytest

from openqlab.io.data_container import DataContainer


def test_constructor_sliced():
    pass


def test_constructor():
    pass


magic_methods = [
    "__add__",
    "__sub__",
    "__mul__",
    "__floordiv__",
    "__truediv__",
    "__mod__",
    "__pow__",
    "__and__",
    "__xor__",
    "__or__",
    "__iadd__",
    "__isub__",
    "__imul__",
    "__ifloordiv__",
    "__itruediv__",
    "__imod__",
    "__ipow__",
    "__iand__",
    "__ixor__",
    "__ior__",
]

header1 = {
    "test1": 1,
    "test2": "string",
}
header2 = {
    "test1": 1,
    "test3": "string2",
}
data1 = [1, 2, 3, 4]
data2 = [5, 6, 7, 8]


@pytest.mark.parametrize("method", magic_methods)
def test_math_magics(method):
    """
    This should test the math magic methods like __add__, __sub__ ...
    """
    dc1 = DataContainer(data=data1, header=header1)
    dc2 = DataContainer(data=data2, header=header2)
    bound_method = dc1.__getattr__(method)
    dc3 = bound_method(dc2)
    assert type(dc3) == DataContainer
    assert isinstance(dc3, DataContainer)
    assert dc3.attrs == {
        "test1": 1
    }  # todo: currently, the combination of DataContainers only uses the common elements in the header dict.
    # Should this be the behaviour when adding dataframes?
    # assert bound_method.__doc__ is None


class TestDocstring:
    default_methods = [
        "asfreq",
        "shift",
        "slice_shift",
        "tshift",
        "to_period",
        "to_timestamp",
        "tz_convert",
        "tz_localize",
    ]

    metaclass_methods = [
        "add",
        "sub",
        "mul",
        "div",
        "divide",
        "truediv",
        "floordiv",
        "mod",
        "pow",
        "dot",
        "radd",
        "rsub",
        "rmul",
        "rdiv",
        "rtruediv",
        "rfloordiv",
        "rmod",
        "rpow",
        "join",
        "merge",
        "combine",
        "combine_first",
        "isin",
        "apply",
        "agg",
        "aggregate",
        "transform",
        "corrwith",
        "reindex_like",
        "pivot",
        "melt",
        "asof",
    ]
    dc1 = DataContainer(data=data1, header=header1)

    @pytest.mark.parametrize("method", metaclass_methods)
    def test_metaclass_inheritance(self, method):
        """
        This test looks for the docstring of methods that are overwritten by the metaclass.
        It checks if the docstring still contains the word DataFrame instead of DataContainer
        """
        doc = self.dc1.__getattr__(method).__doc__
        assert doc is not None
        assert doc
        assert "DataFrame" not in doc
        if "DataContainerSeries" not in doc:
            assert "Series" not in doc
        assert "DataContainer" in doc

    @pytest.mark.xfail(
        reason="docstring replacement for normally inherited methods is not implemented yet"
    )
    @pytest.mark.parametrize("method", default_methods)
    def test_default_inheritance(self, method):
        """
        This test looks for the docstring of methods that are not overwritten by the metaclass but regularly inherited.
        It checks if the docstring still contains the word DataFrame instead of DataContainer
        """
        doc = self.dc1.__getattr__(method).__doc__
        assert doc is not None
        assert doc
        assert "DataFrame" not in doc
        assert "DataContainer" in doc

    @pytest.mark.xfail(
        reason="docstring replacement for normally inherited methods is not implemented yet"
    )
    @pytest.mark.parametrize("method", dir(DataContainer))
    def test_all(self, method):
        doc = self.dc1.__getattr__(method).__doc__
        assert "DataFrame" not in doc
        if "DataContainerSeries" not in doc:
            assert "Series" not in doc
        # assert "DataContainer" in doc
