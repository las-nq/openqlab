from pathlib import Path

from openqlab.io.data_container import DataContainer

filepath = Path(__file__).parent

header = {"comment": "this is a comment", "value": 1}
original = DataContainer(data=[5, 4, 3, 2, 1], header=header)


def test_to_csv_with_kwargs():
    filename = filepath / "with_kwargs.csv"
    original.to_csv(filename, encoding=None, sep=",")

    loaded = DataContainer.from_csv(filename)
    assert loaded.header == original.header
    assert (loaded.values == original.values).all()


def test_to_csv_with_header():
    filename = filepath / "with_header.csv"
    original.to_csv(filename)

    loaded = DataContainer.from_csv(filename)
    assert loaded.header == original.header
    assert (loaded.values == original.values).all()


def test_to_csv_no_header():
    filename = filepath / "no_header.csv"
    original.to_csv(filename, header=False)

    loaded = DataContainer.from_csv(filename, header=None)
    assert loaded.header == {}
    assert (loaded.values == original.values).all()


def test_from_csv_header():
    filename = filepath / "with_header.csv"
    loaded = DataContainer.from_csv(filename, header=0)
    assert loaded.header == {}
    assert (loaded.values == original.values).all()


def test_append_file(tmp_path):
    d = tmp_path / "data.csv"
    original.to_csv(d)
    with open(d) as file:
        lines = file.read().splitlines()
    assert lines == [
        "-----DataContainerHeader",
        '{"comment": "this is a comment", "value": 1}',
        "-----DataContainerData",
        ",0",
        "0,5",
        "1,4",
        "2,3",
        "3,2",
        "4,1",
    ]
    original.to_csv(d, mode="a", header=False)
    with open(d) as file:
        lines = file.read().splitlines()
    assert lines == [
        "-----DataContainerHeader",
        '{"comment": "this is a comment", "value": 1}',
        "-----DataContainerData",
        ",0",
        "0,5",
        "1,4",
        "2,3",
        "3,2",
        "4,1",
        "0,5",
        "1,4",
        "2,3",
        "3,2",
        "4,1",
    ]


def test_column_names_given(tmp_path):
    d = tmp_path / "data.csv"
    original.to_csv(d, header=["column1"])
    with open(d) as file:
        lines = file.read().splitlines()
    assert lines == [
        "-----DataContainerHeader",
        '{"comment": "this is a comment", "value": 1}',
        "-----DataContainerData",
        ",column1",
        "0,5",
        "1,4",
        "2,3",
        "3,2",
        "4,1",
    ]
