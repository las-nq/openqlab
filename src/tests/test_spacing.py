import numpy as np

from openqlab.spacing import stepspace


def test_stepspace_general():
    assert all(
        stepspace(0, 5, 10) == [0, 5, 10, 15, 20, 25, 30, 35, 40, 45]
    ), "Positive start value and integer step not working"
    assert all(
        stepspace(-20, 5, 10) == [-20, -15, -10, -5, 0, 5, 10, 15, 20, 25]
    ), "Negative start value not working"
    assert all(
        stepspace(0, 1.25, 5) == [0, 1.25, 2.5, 3.75, 5]
    ), "Float stepsize not working"
    assert all(stepspace(1, 3, 1) == [1]), "num=1 not working"


def test_arrays_as_inputs():
    result = stepspace(1, np.array([1, 2]), 5, axis=1)
    expected = np.array([[1, 2, 3, 4, 5], [1, 3, 5, 7, 9]], dtype=float)
    assert (result == expected).all(), "start array not working"

    result = stepspace(np.array([1, 2]), np.array([1, 2]), 3, axis=1)
    expected = np.array(
        [
            [1, 2, 3],
            [2, 4, 6],
        ],
        dtype=float,
    )
    assert (result == expected).all(), "array in start and step not working"
