import unittest

import numpy as np
import pandas as pd

from openqlab.conversion.db import mean


class TestMean(unittest.TestCase):
    def setUp(self) -> None:
        self.input_list = [10, 30, 50]
        self.input_list2 = [-10, 10, 3]
        self.output_mean_axis_0 = [45.27243116388089, 6.054940368048718]
        self.output_mean_axis_1 = [
            7.032913781186614,
            27.0329137811866150,
            46.98978669563705,
        ]
        self.output_mean_all = 42.26265121429045

    def test_dataframe_axis_None(self):
        input_df = pd.DataFrame({"first": self.input_list, "second": self.input_list2})
        expected_result = pd.Series(
            self.output_mean_axis_0, index=["first", "second"], dtype="float64"
        )
        result = mean(input_df)
        pd.testing.assert_series_equal(result, expected_result)

    def test_dataframe_axis_0(self):
        input_df = pd.DataFrame({"first": self.input_list, "second": self.input_list2})
        expected_result = pd.Series(
            self.output_mean_axis_0, index=["first", "second"], dtype="float64"
        )
        result = mean(input_df, axis=0)
        pd.testing.assert_series_equal(result, expected_result)

    def test_dataframe_axis_1(self):
        input_df = pd.DataFrame({"first": self.input_list, "second": self.input_list2})
        expected_result = pd.Series(
            self.output_mean_axis_1, index=[0, 1, 2], dtype="float64"
        )
        result = mean(input_df, axis=1)
        pd.testing.assert_series_equal(result, expected_result)

    def test_numpy_array_axis_None(self):
        input_array = np.array([self.input_list, self.input_list2]).T
        expected_result = np.array(self.output_mean_all)
        result = mean(input_array)
        np.testing.assert_array_equal(result, expected_result)

    def test_numpy_array_axis_0(self):
        input_array = np.array([self.input_list, self.input_list2]).T
        expected_result = np.array(self.output_mean_axis_0)
        result = mean(input_array, axis=0)
        np.testing.assert_array_equal(result, expected_result)

    def test_numpy_array_axis_1(self):
        input_array = np.array([self.input_list, self.input_list2]).T
        expected_result = np.array(self.output_mean_axis_1)
        result = mean(input_array, axis=1)
        np.testing.assert_array_equal(result, expected_result)
