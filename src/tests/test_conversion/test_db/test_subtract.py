import unittest

import numpy as np
import pandas as pd

from openqlab import DataContainer
from openqlab.conversion.db import subtract


class TestSubtract(unittest.TestCase):
    def test_int(self):
        self.assertAlmostEqual(subtract(20, 10), 19.54242509439325)
        self.assertAlmostEqual(subtract(20, -10), 19.995654882259824)
        self.assertAlmostEqual(subtract(10, 5), 8.349114613732302)

    def test_float(self):
        self.assertAlmostEqual(subtract(20.5, 10.8), 20.007775639258234)
        self.assertAlmostEqual(subtract(20.5, -10.8), 20.496779336625327)
        self.assertAlmostEqual(subtract(10.3, 5.7), 8.450881604099695)

        self.assertAlmostEqual(subtract(20, 10.8), 19.44370904194485)
        self.assertAlmostEqual(subtract(20, -10.8), 19.996386192714212)
        self.assertAlmostEqual(subtract(10, 5.7), 7.982809379785114)

    def test_numpy_array(self):
        signal = np.array([20.5, 20.5, 10.3])
        noise = np.array([10.8, -10.8, 5.7])
        expected_result = np.array(
            [20.007775639258234, 20.496779336625327, 8.450881604099695]
        )
        result = subtract(signal, noise)
        np.testing.assert_array_almost_equal(result, expected_result)

    def test_multi_numpy_array(self):
        signal = np.array([[20.5, 20.5, 10.3], [20, 20, 10]])
        noise = np.array([[10.8, -10.8, 5.7], [10, -10, 5]])
        expected_result = np.array(
            [
                [20.007775639258234, 20.496779336625327, 8.450881604099695],
                [19.54242509439325, 19.995654882259824, 8.349114613732302],
            ]
        )
        result = subtract(signal, noise)
        np.testing.assert_array_almost_equal(result, expected_result)

    def test_two_numpy_signal_array(self):
        signal = np.array([[20.5, 20.5, 10.3], [20, 20, 10]])
        noise = np.array([10.8, -10.8, 5.7])
        expected_result = np.array(
            [
                [20.007775639258234, 20.496779336625327, 8.450881604099695],
                [19.44370904194485, 19.996386192714212, 7.982809379785114],
            ]
        )
        result = subtract(signal, noise)
        np.testing.assert_array_almost_equal(result, expected_result)

    def test_dataframe(self):
        data = pd.DataFrame(
            {
                "signal1": [20.5, 20.5, 10.3],
                "signal2": [20, 20, 10],
                "noise": [10.8, -10.8, 5.7],
            }
        )
        expected_result = pd.DataFrame(
            {
                "signal1": [20.00777563, 20.4967793, 8.4508816],
                "signal2": [19.4437090, 19.9963861, 7.9828093],
                "noise": [-np.inf, -np.inf, -np.inf],
            }
        )
        result = subtract(data, data["noise"])
        pd.testing.assert_frame_equal(result, expected_result)

    def test_datacontainer(self):
        data = DataContainer(
            {
                "signal1": [20.5, 20.5, 10.3],
                "signal2": [20, 20, 10],
                "noise": [10.8, -10.8, 5.7],
            }
        )
        expected_result = DataContainer(
            {
                "signal1": [20.007775639258234, 20.496779336625327, 8.450881604099695],
                "signal2": [19.44370904194485, 19.996386192714212, 7.982809379785114],
                "noise": [-np.inf, -np.inf, -np.inf],
            }
        )
        result = subtract(data, data["noise"])
        pd.testing.assert_frame_equal(result, expected_result)
