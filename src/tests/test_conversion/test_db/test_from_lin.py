import unittest

import numpy as np
import pandas as pd

from openqlab.conversion.db import from_lin


class TestFromLin(unittest.TestCase):
    def test_positive_integer(self):
        self.assertEqual(from_lin(10), 10)
        self.assertEqual(from_lin(100000), 50)
        self.assertAlmostEqual(from_lin(1.9952623), 3)

    def test_negative_integer(self):
        self.assertEqual(from_lin(0.1), -10)
        self.assertEqual(from_lin(1e-5), -50)
        self.assertAlmostEqual(from_lin(0.5011872336272722), -3)

    def test_positive_float(self):
        self.assertAlmostEqual(from_lin(2.23872113), 3.5)

    def test_negative_float(self):
        self.assertAlmostEqual(from_lin(0.446683592), -3.5)

    def test_dataframe(self):
        df = pd.DataFrame([10, 0.1, 1e5])
        expected_df = pd.DataFrame([10, -10, 50], dtype="float64")
        pd.testing.assert_frame_equal(from_lin(df), expected_df)

    def test_numpy_array(self):
        a = np.array([10, 0.1, 1e5])
        expected_array = np.array([10, -10, 50])
        np.testing.assert_array_equal(from_lin(a), expected_array)
