import unittest

import numpy as np
import pandas as pd

from openqlab.conversion.db import to_lin


class TestToLin(unittest.TestCase):
    def test_positive_integer(self):
        self.assertEqual(to_lin(10), 10)
        self.assertEqual(to_lin(50), 100000)
        self.assertAlmostEqual(to_lin(3), 1.9952623)

    def test_negative_integer(self):
        self.assertEqual(to_lin(-10), 0.1)
        self.assertEqual(to_lin(-50), 1e-5)
        self.assertAlmostEqual(to_lin(-3), 0.5011872336272722)

    def test_positive_float(self):
        self.assertAlmostEqual(to_lin(3.5), 2.23872113)

    def test_negative_float(self):
        self.assertAlmostEqual(to_lin(-3.5), 0.446683592)

    def test_dataframe(self):
        df = pd.DataFrame([10, -10, 50])
        expected_df = pd.DataFrame([10, 0.1, 1e5])
        pd.testing.assert_frame_equal(to_lin(df), expected_df)

    def test_numpy_array(self):
        a = np.array([10, -10, 50])
        expected_array = np.array([10, 0.1, 1e5])
        np.testing.assert_array_equal(to_lin(a), expected_array)
