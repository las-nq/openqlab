import unittest

from openqlab.conversion.utils import human_readable


class TestHumanReadable(unittest.TestCase):
    def setUp(self):
        pass

    def test_negativ_value(self):
        result = human_readable(-2e-3, unit="m")
        self.assertEqual("-2 mm", result)

        result = human_readable(-2.1e-3, unit="m")
        self.assertEqual("-2.1 mm", result)

    def test_comma_value(self):
        result = human_readable(2.1e-3, unit="m")
        self.assertEqual("2.1 mm", result)

        result = human_readable(2.13e-9, unit="m")
        self.assertEqual("2.13 nm", result)

        result = human_readable(2.135e12, unit="m")
        self.assertEqual("2.135 Tm", result)

        result = human_readable(2.1357e3, unit="m")
        self.assertEqual("2.136 km", result)

        result = human_readable(2.13579e3, unit="m")
        self.assertEqual("2.136 km", result)

    def test_yotta(self):
        result = human_readable(2e28, unit="m")
        self.assertEqual("2e+28 m", result)

        result = human_readable(2e27, unit="m")
        self.assertEqual("2e+27 m", result)

        result = human_readable(2e26, unit="m")
        self.assertEqual("200 Ym", result)

        result = human_readable(2e25, unit="m")
        self.assertEqual("20 Ym", result)

        result = human_readable(2e24, unit="m")
        self.assertEqual("2 Ym", result)

    def test_zetta(self):
        result = human_readable(2e23, unit="m")
        self.assertEqual("200 Zm", result)

        result = human_readable(2e22, unit="m")
        self.assertEqual("20 Zm", result)

        result = human_readable(2e21, unit="m")
        self.assertEqual("2 Zm", result)

    def test_exa(self):
        result = human_readable(2e20, unit="m")
        self.assertEqual("200 Em", result)

        result = human_readable(2e19, unit="m")
        self.assertEqual("20 Em", result)

        result = human_readable(2e18, unit="m")
        self.assertEqual("2 Em", result)

    def test_peta(self):
        result = human_readable(2e17, unit="m")
        self.assertEqual("200 Pm", result)

        result = human_readable(2e16, unit="m")
        self.assertEqual("20 Pm", result)

        result = human_readable(2e15, unit="m")
        self.assertEqual("2 Pm", result)

    def test_tera(self):
        result = human_readable(2e14, unit="m")
        self.assertEqual("200 Tm", result)

        result = human_readable(2e13, unit="m")
        self.assertEqual("20 Tm", result)

        result = human_readable(2e12, unit="m")
        self.assertEqual("2 Tm", result)

    def test_giga(self):
        result = human_readable(2e11, unit="m")
        self.assertEqual("200 Gm", result)

        result = human_readable(2e10, unit="m")
        self.assertEqual("20 Gm", result)

        result = human_readable(2e9, unit="m")
        self.assertEqual("2 Gm", result)

    def test_mega(self):
        result = human_readable(2e8, unit="m")
        self.assertEqual("200 Mm", result)

        result = human_readable(2e7, unit="m")
        self.assertEqual("20 Mm", result)

        result = human_readable(2e6, unit="m")
        self.assertEqual("2 Mm", result)

    def test_kilo(self):
        result = human_readable(2e5, unit="m")
        self.assertEqual("200 km", result)

        result = human_readable(2e4, unit="m")
        self.assertEqual("20 km", result)

        result = human_readable(2e3, unit="m")
        self.assertEqual("2 km", result)

    def test_hecto(self):
        result = human_readable(2e2, unit="m", tenth_steps=True)
        self.assertEqual("2 hm", result)

    def test_deka(self):
        result = human_readable(2e1, unit="m", tenth_steps=True)
        self.assertEqual("2 dam", result)

    def test_no_prefix(self):
        result = human_readable(2e2, unit="m")
        self.assertEqual("200 m", result)

        result = human_readable(2e1, unit="m")
        self.assertEqual("20 m", result)

        result = human_readable(2e0, unit="m")
        self.assertEqual("2 m", result)

    def test_deci(self):
        result = human_readable(2e-1, unit="m", tenth_steps=True)
        self.assertEqual("2 dm", result)

    def test_centi(self):
        result = human_readable(2e-2, unit="m", tenth_steps=True)
        self.assertEqual("2 cm", result)

    def test_milli(self):
        result = human_readable(2e-1, unit="m")
        self.assertEqual("200 mm", result)

        result = human_readable(2e-2, unit="m")
        self.assertEqual("20 mm", result)

        result = human_readable(2e-3, unit="m")
        self.assertEqual("2 mm", result)

    def test_micro(self):
        result = human_readable(2e-4, unit="m")
        self.assertEqual("200 µm", result)

        result = human_readable(2e-5, unit="m")
        self.assertEqual("20 µm", result)

        result = human_readable(2e-6, unit="m")
        self.assertEqual("2 µm", result)

    def test_nano(self):
        result = human_readable(2e-7, unit="m")
        self.assertEqual("200 nm", result)

        result = human_readable(2e-8, unit="m")
        self.assertEqual("20 nm", result)

        result = human_readable(2e-9, unit="m")
        self.assertEqual("2 nm", result)

    def test_pico(self):
        result = human_readable(2e-10, unit="m")
        self.assertEqual("200 pm", result)

        result = human_readable(2e-11, unit="m")
        self.assertEqual("20 pm", result)

        result = human_readable(2e-12, unit="m")
        self.assertEqual("2 pm", result)

    def test_femto(self):
        result = human_readable(2e-13, unit="m")
        self.assertEqual("200 fm", result)

        result = human_readable(2e-14, unit="m")
        self.assertEqual("20 fm", result)

        result = human_readable(2e-15, unit="m")
        self.assertEqual("2 fm", result)

    def test_atto(self):
        result = human_readable(2e-16, unit="m")
        self.assertEqual("200 am", result)

        result = human_readable(2e-17, unit="m")
        self.assertEqual("20 am", result)

        result = human_readable(2e-18, unit="m")
        self.assertEqual("2 am", result)

    def test_zepto(self):
        result = human_readable(2e-19, unit="m")
        self.assertEqual("200 zm", result)

        result = human_readable(2e-20, unit="m")
        self.assertEqual("20 zm", result)

        result = human_readable(2e-21, unit="m")
        self.assertEqual("2 zm", result)

    def test_yocto(self):
        result = human_readable(2e-22, unit="m")
        self.assertEqual("200 ym", result)

        result = human_readable(2e-23, unit="m")
        self.assertEqual("20 ym", result)

        result = human_readable(2e-24, unit="m")
        self.assertEqual("2 ym", result)

        result = human_readable(2e-25, unit="m")
        self.assertEqual("2e-25 m", result)

        result = human_readable(2e-26, unit="m")
        self.assertEqual("2e-26 m", result)
