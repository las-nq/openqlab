import unittest

from openqlab.analysis.electromagnetic_wave import ElectromagneticWave
from openqlab.conversion import Wavelength


class TestWavelength(unittest.TestCase):
    def test_deprecation_warning(self):
        lambda0 = 1e-6
        with self.assertWarns(DeprecationWarning):
            Wavelength(lambda0)

    def test_instanciation(self):
        wave = Wavelength(1e-6)
        self.assertIsInstance(wave, ElectromagneticWave)
        self.assertEqual(1e-6, wave.wavelength)
