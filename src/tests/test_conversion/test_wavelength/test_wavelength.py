import math
import unittest

from scipy import constants as const

from openqlab.conversion import wavelength


class TestWavelength(unittest.TestCase):
    def test_responsivity(self):
        lambda0 = 1e-6
        l = wavelength.Wavelength(lambda0)
        self.assertEqual(l.quantum_efficiency(l.ideal_responsivity), 1.0)
        self.assertEqual(l.ideal_responsivity, 1 / l.eV)

    def test_conversion(self):
        lambda0 = 1.064e-6
        l = wavelength.Wavelength(lambda0)
        self.assertEqual(lambda0, l.lambda0)
        self.assertEqual(lambda0, l.wavelength)
        self.assertEqual(l.f, const.c / lambda0)
        self.assertAlmostEqual(l.wavenumber, 9398.4962406)

        l2 = wavelength.Wavelength.from_f(const.c / lambda0)
        self.assertAlmostEqual(l.lambda0, l2.lambda0)

        l2 = wavelength.Wavelength.from_omega(2 * math.pi * const.c / lambda0)
        self.assertAlmostEqual(l.lambda0, l2.lambda0)

        self.assertAlmostEqual(l.lambda0, wavelength.Wavelength.from_eV(l.eV).lambda0)

    def test_shotnoise(self):
        lambda0 = 1.064e-6
        l = wavelength.Wavelength(lambda0)
        self.assertAlmostEqual(l.RIN(1e-3) / 1.9323e-8, 1.0, places=4)


if __name__ == "__main__":
    unittest.main()
