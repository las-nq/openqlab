import unittest
from pathlib import Path

import matplotlib

from openqlab import io, plots

filedir = Path(__file__).parent
datadir = filedir / "data"


class TestTimeDomain(unittest.TestCase):
    def setUp(self):
        self.data = io.read(fr"{datadir}/20180606_001.csv")

    def test_default_plot(self):
        fig = plots.scope(self.data)
        fig = plots.scope(self.data.iloc[:, 0])
        self.assertIsInstance(fig, matplotlib.figure.Figure)
