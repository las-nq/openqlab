import logging
import unittest
import warnings
from copy import deepcopy
from pathlib import Path
from typing import List

from openqlab.io.data_container import DataContainer

log = logging.getLogger(__name__)


class TestDeprecationWarnings(unittest.TestCase):
    def test_data_container_header_type(self):
        with self.assertWarns(DeprecationWarning):
            DataContainer(type="osci")
