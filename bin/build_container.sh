#!/usr/bin/env bash
set -e

docker=docker
CI_REGISTRY=docker.io
project=openqlab
main_version=3.13
# basename="$CI_REGISTRY/lasnq"
basename="lasnq"

build_container () {
    python_version=$1
    tag=$2

    echo "Python version: $python_version $1"

    $docker build --build-arg PYTHON_VERSION=$python_version \
                 -t $basename/$project:$tag .
}


for version in 3.10 3.12; do
    echo "build for python $version"
    build_container $version $version
    $docker push $basename/$project:$version
done

build_container $main_version develop
$docker push $basename/$project:develop

# Make upload for public gitlab.com CI
$docker tag $basename/$project:develop lasnq/$project
$docker push lasnq/$project
