#!/bin/zsh
while true
do
  inotifywait -e modify src/**/*.py
  make mypy && make test
done
